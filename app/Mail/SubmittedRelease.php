<?php

namespace App\Mail;

use App\Models\Release;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Mail\Mailables\Address;
use Illuminate\Mail\Mailables\Content;

class SubmittedRelease extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct(
      protected Release $release,
    ) {}

    public function envelope(): Envelope
    {
        return new Envelope(
            from: new Address('novosreleases@wgo.com.br', 'WGO'),
            subject: 'Release submetido com sucesso',
        );
    }

    public function content(): Content
    {
        $title = 'Seu release '. $this->release->name . ' foi enviado com sucesso!';

        return new Content(
            view: 'mails.submittedRelease',
            with: [
                'title' => $title,
                'release' => $this->release,
            ],
        );
    }

    public function build()
    {
        $title = 'Seu release '.$this->release->name.' foi enviado com sucesso!';


        return new Content(
            view: 'mails.submittedRelease',
            with: [
                'title' => $title,
                'release' => $this->release,
            ],
        );
    }
}
