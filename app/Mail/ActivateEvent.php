<?php

namespace App\Mail;

use App\Models\Event;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Mail\Mailables\Address;
use Illuminate\Mail\Mailables\Content;

class ActivateEvent extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct(
      protected Event $event,
    ) {}

    public function envelope(): Envelope
    {
        return new Envelope(
            from: new Address('ativacao@wgo.com.br', 'WGO'),
            subject: 'Evento ativado com sucesso',
        );
    }

    public function content(): Content
    {
        $title = 'Seu evento '.$this->event->name.' foi ativado com sucesso!';

        return new Content(
            view: 'mails.activatedEvent',
            with: [
                'title' => $title,
                'event' => $this->event,
            ],
        );
    }

    public function build()
    {
        $title = 'Seu evento '.$this->event->name.' foi ativado com sucesso!';

        return new Content(
            view: 'mails.activatedEvent',
            with: [
                'title' => $title,
                'event' => $this->event,
            ],
        );
    }
}
