<?php

namespace App\Mail;

use App\Models\Release;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Mail\Mailables\Address;
use Illuminate\Mail\Mailables\Content;

class ActivateRelease extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct(
      protected Release $release,
    ) {}

    public function envelope(): Envelope
    {
        return new Envelope(
            from: new Address('ativacao@wgo.com.br', 'WGO'),
            subject: 'release ativado com sucesso',
        );
    }

    public function content(): Content
    {
        $title = 'Seu release '.$this->release->name.' foi ativado com sucesso!';

        return new Content(
            view: 'mails.activatedRelease',
            with: [
                'title' => $title,
                'release' => $this->release,
            ],
        );
    }

    public function build()
    {
        $title = 'Seu release '.$this->release->name.' foi ativado com sucesso!';

        return new Content(
            view: 'mails.activatedRelease',
            with: [
                'title' => $title,
                'release' => $this->release,
            ],
        );
    }
}
