<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
Use App\Models\Photo;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class FileUploadController extends Controller
{
    public function uploadImage(Request $request)
    {
        try {
            if (isset($_SERVER['HTTPS'])) {
                $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
            } else {
                $protocol = 'http';
            }
    
            if ($request->hasFile('image') && $request->file('image')->isValid()) {
                $image = $request->file('image');
                $filePath = public_path('images/' . $request['filePath']);
                $originalName = $image->getClientOriginalName();
                $fileName = pathinfo($originalName, PATHINFO_FILENAME);
                $extension = $image->getClientOriginalExtension();
                $imageName = $fileName . '.' . $extension;
                $imagePath = $filePath . '/' . $imageName;
    
                if (file_exists($imagePath)) {
                    $counter = 1;
                    while (file_exists($imagePath)) {
                        $imageName = $fileName . '_' . $counter . '.' . $extension;
                        $imagePath = $filePath . '/' . $imageName;
                        $counter++;
                    }
                }
    
                if (!is_dir($filePath)) {
                    mkdir($filePath, 0755, true);
                }
    
                if (!is_writable($filePath)) {
                    throw new \Exception("Diretório de destino não tem permissões de escrita.");
                }
    
                $image->move($filePath, $imageName);
    
                $file = $filePath . '/' . $imageName;
                $this->resizeImage($file, $request['filePath'], 'thumb', 100, 100);
                $this->resizeImage($file, $request['filePath'], 'medium', 300, 300);
                $this->resizeImage($file, $request['filePath'], 'large', 500, 500);
                $this->resizeImage($file, $request['filePath'], 'full', 1000, 1000);
    
                return response()->json($protocol . "://" . $_SERVER['HTTP_HOST'] . '/images/' . $request['filePath'] . '/' . $imageName);
            } else {
                throw new \Exception("Arquivo de imagem inválido.");
            }
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'exception' => get_class($e)], 500);
        }
    }

    private function resizeImage($file, $filePath, $size, $width, $height)
    {
        
        $img = Image::make($file);
        $img->fit($width, $height);
        $img->save(public_path("images/{$filePath}/"  . $size . '_' . basename($file)));
    }

    public function uploadExtraImages(Request $request)
    {
        if(isset($_SERVER['HTTPS'])){
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        }
        else{
            $protocol = 'http';
        }
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $image = $request->file('image');
            $filePath = public_path('images/' . $request['filePath']);
            // $imageName = $image->getClientOriginalName();
            $imageName = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME)  . '_' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/' . $request['filePath']), $imageName);
            return response()->json($protocol . "://" .$_SERVER['HTTP_HOST'] . '/images/' . $request['filePath'] . '/' . $imageName);
        }
            
    }
}