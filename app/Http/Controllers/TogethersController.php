<?php

namespace App\Http\Controllers;

use App\Models\Together;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;

class TogethersController extends Controller
{
    public function index()
    {
        return Inertia::render('Together/List');
    }

    public function edit(Request $request)
    {
        $together = Together::with(['category'])->findOrFail($request['together']);
        return Inertia::render('Together/Edit', [
            'together' => $together,
        ]);
    }

    public function create(Request $request)
    {
        return Inertia::render('Together/Create');
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $together = Together::find($request['id']);
        $together->update($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $together = Together::create($data);

        return response()->json($together);
    }

    public function destroy(Request $request)
    {
        $together = Together::find($request['together']);
        $together->delete();
    }
}
