<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Models\State;
use App\Models\City;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;
use App\Helpers\AddVideoEmbedTags;

class LocationsController extends Controller
{
    public function index()
    {
        return Inertia::render('Location/List');
    }

    public function edit(Request $request)
    {

        $location = Location::with(['city', 'city.state'])->findOrFail($request['location']);

        return Inertia::render('Location/Edit', [
            'location' => $location,
            'states' => State::all(),
            // 'cities' => City::all()
        ]);
    }

    public function create(Request $request)
    {
        return Inertia::render('Location/Create', [
            'states' => State::all(),
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        array_key_exists('description', $data) && $data['description'] = AddVideoEmbedTags::addTags($data['description']);
        $location = Location::create($data);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        array_key_exists('description', $data) && $data['description'] = AddVideoEmbedTags::addTags($data['description']);
        $location = Location::find($request['id']);
        $location->update($data);

    }


    public function uploadImage(Request $request)
    {
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalName();
            $image->move(public_path('images'), $imageName);
            return response()->json($imageName);
        }

        return response()->json('Erro ao fazer upload da imagem.', 500);
    }

    public function destroy(Request $request)
    {
        $location = Location::find($request['location']);
        $location->delete();
    }

}
