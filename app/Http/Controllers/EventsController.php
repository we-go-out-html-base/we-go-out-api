<?php

namespace App\Http\Controllers;


use App\Models\Event;
use App\Models\EventType;
use App\Models\Artist;
use App\Models\Genre;
use App\Models\Together;
use App\Models\Location;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Mail\ActivateEvent;
use App\Models\News;
use Illuminate\Support\Facades\DB;
use App\Helpers\AddVideoEmbedTags;
use App\Helpers\HandleSlugString;
use App\Helpers\RemoveDiacritic;
use App\Helpers\RemoveStyleForAmp;
use App\Http\Requests\StoreEventRequest;
use App\Http\Requests\UpdateEventRequest;

class EventsController extends Controller
{
    public function index()
    {
        return Inertia::render('Events/List');
    }

    public function edit(Request $request)
    {
        $evento = Event::with(['artists', 'togethers', 'linkedEvents'])->findOrFail($request['event']);
        return Inertia::render('Events/Edit', [
            'evento' => $evento,
            'togethers' => Together::all(),
            'locations' => Location::with(['city', 'city.state'])->get(),
            'artists' => Artist::all(),
            'events' => Event::where('id', '!=', $evento->id)->get(),
            'eventTypes' => EventType::all(),
            'review' => News::find($evento->review_id)
        ]);
    }

    public function create(Request $request)
    {
        return Inertia::render(
            'Events/Create',
            [
                'togethers' => Together::all(),
                'locations' => Location::with(['city', 'city.state'])->get(),
                'artists' => Artist::all(),
                'events' => Event::all(),
                'eventTypes' => EventType::all(),
                // 'review' => []
            ]
        );
    }

    public function store(StoreEventRequest $request)
    {
        $data = $request->all();

        array_key_exists('about', $data) && $data['about'] =  RemoveStyleForAmp::remove($data['about']);
        array_key_exists('price', $data) && $data['price'] = RemoveStyleForAmp::remove($data['price']);
        array_key_exists('details', $data) && $data['details'] = RemoveStyleForAmp::remove($data['details']);

        $data['slug'] = HandleSlugString::handle($data['slug']);
        $data['slug'] = RemoveDiacritic::remove($data['slug']);

        array_key_exists('about', $data) && $data['about'] && $data['about'] = AddVideoEmbedTags::addTags($data['about']);
        array_key_exists('price', $data) && $data['price'] = AddVideoEmbedTags::addTags($data['price']);
        array_key_exists('details', $data) && $data['details'] = AddVideoEmbedTags::addTags($data['details']);

        $event = Event::create($data);
        $event->artists()->sync($data['artists']);
        $event->togethers()->sync($data['togethers']);
        $event->linkedEvents()->sync($data['linked_events']);

        return response()->json($event);
    }

    public function update(UpdateEventRequest $request)
    {
        $data = $request->all();

        $verificaslug = Event::where('slug', $data['slug'])->where('id', '<>', $data['id'])->count();

        if ($verificaslug > 0) {
            return response()->json(['error' => 'O slug já está sendo usado por outro conteúdo'], 422);
        }

        array_key_exists('about', $data) && $data['about'] = RemoveStyleForAmp::remove($data['about']);
        array_key_exists('price', $data) && $data['price'] = RemoveStyleForAmp::remove($data['price']);
        array_key_exists('details', $data) && $data['details'] = RemoveStyleForAmp::remove($data['details']);

        $data['slug'] = HandleSlugString::handle($data['slug']);
        $data['slug'] = RemoveDiacritic::remove($data['slug']);

        array_key_exists('about', $data) && $data['about'] && $data['about'] = AddVideoEmbedTags::addTags($data['about']);
        array_key_exists('price', $data) && $data['price'] = AddVideoEmbedTags::addTags($data['price']);
        array_key_exists('details', $data) && $data['details'] = AddVideoEmbedTags::addTags($data['details']);

        $event = Event::find($request['event']);
        $event->update($data);
        $event->artists()->sync($data['artists']);
        $event->togethers()->sync($data['togethers']);
        $event->linkedEvents()->sync($data['linked_events']);
        return response()->json(['message' => 'Evento atualizado com sucesso']);
    }

    public function destroy(Request $request)
    {
        $event = Event::find($request['event']);
        $event->delete();
    }

    public function activate(Request $request)
    {
        $data = $request->all();
        $event = Event::find($data['id']);
        $event->is_draft = false;
        $event->save();

        // $isSiteSend = DB::table('site_senders')->where('type', 'event')->where('id', $data['id'])->exists();
        // if ($isSiteSend) {
        //     try {
        //         $sender = DB::table('site_senders')->where('type', 'event')->where('id', $data['id'])->first();
        //         Mail::to($sender->email)->send(new ActivateEvent($event));
        //     } catch (Throwable $e) {
        //         return false;
        //     }
        // }
    }
}
