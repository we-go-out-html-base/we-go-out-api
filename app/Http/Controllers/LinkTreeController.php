<?php

namespace App\Http\Controllers;

use App\Models\LinkTree;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;

class LinkTreeController extends Controller
{
    public function index()
    {
        return Inertia::render('Links/List');
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $link = LinkTree::findorfail($request['linktree']);
        $link->update($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $linktree = LinkTree::create($data);
        return $linktree;
    }

    public function destroy(Request $request)
    {
        $link = LinkTree::findorfail($request['linktree']);
        $link->delete();
    }
}
