<?php
namespace App\Http\Controllers;

use App\Http\Requests\CityRequest;
use App\Http\Requests\StateRequest;
use App\Models\City;
use App\Models\State;
use App\Models\Country;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;

class CitiesController extends Controller
{

    public function index()
    {
        return Inertia::render('City/List');
    }

    public function edit(Request $request)
    {
        $city = City::findOrFail($request['city']);

        return Inertia::render('City/Edit', [
            'city' => $city,
            'statesList' => State::with('country')->get()
        ]);
    }

    public function update(CityRequest $request)
    {
        $data = $request->all();
        $city = City::find($request['city']);
        $city->update($data);
    }

    public function create()
    {
        return Inertia::render('City/Create');
    }

    public function store(CityRequest $request)
    {
        $data = $request->all();
        $city = City::create($data);
    }

    public function destroy(Request $request)
    {
        $city = City::find($request['city']);
        $city->delete();
    }




}
