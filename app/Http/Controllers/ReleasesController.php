<?php

namespace App\Http\Controllers;


use App\Models\Event;
use App\Models\Artist;
use App\Models\Company;
use App\Models\Genre;
use App\Models\Release;
use App\Models\TypeRelease;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Mail\ActivateRelease;
use Illuminate\Support\Facades\DB;
use App\Helpers\AddVideoEmbedTags;
use App\Helpers\HandleSlugString;
use App\Helpers\RemoveDiacritic;
use App\Http\Requests\UpdateReleaseRequest;
use App\Http\Requests\StoreReleaseRequest;

class ReleasesController extends Controller
{
    public function index()
    {
        return Inertia::render('Release/List');
    }

    public function edit(Request $request)
    {
        $release = Release::with(['artists', 'companies', 'genres'])->findOrFail($request['release']);
        return Inertia::render('Release/Edit', [
            'release' => $release,
            // 'types' => TypeRelease::all(), // comentado pois foi excluído do escopo mas talvez volte
            'artists' => Artist::all(),
            'companies' => Company::all(),
            'genres' => Genre::all()
        ]);
    }

    public function create(Request $request)
    {
        return Inertia::render('Release/Create', [
            // 'types' => TypeRelease::all(), // comentado pois foi excluído do escopo mas talvez volte
            'artists' => Artist::all(),
            'companies' => Company::all(),
            'genres' => Genre::all()
        ]);
    }

    public function update(UpdateReleaseRequest $request)
    {
        $data = $request->all();

        $verificaslug = Release::where('slug', $data['slug'])->where('id', '<>', $data['id'])->count();

        if ($verificaslug > 0) {
            return response()->json(['error' => 'O slug já está sendo usado por outro conteúdo'], 422);
        }

        $data['slug'] = HandleSlugString::handle($data['slug']);
        $data['slug'] = RemoveDiacritic::remove($data['slug']);

        array_key_exists('description', $data) && $data['description'] = AddVideoEmbedTags::addTags($data['description']);

        $release = Release::find($request['id']);
        $release->update($data);
        $release->artists()->sync($data['artists']);
        $release->companies()->sync($data['companies']);
        $release->genres()->sync($data['genres']);

        return response()->json(['message' => 'Evento atualizado com sucesso']);
    }

    public function store(StoreReleaseRequest $request)
    {
        $data = $request->all();

        $data['slug'] = HandleSlugString::handle($data['slug']);
        $data['slug'] = RemoveDiacritic::remove($data['slug']);
        array_key_exists('description', $data) && $data['description'] = AddVideoEmbedTags::addTags($data['description']);

        $release = Release::create($data);
        $release->artists()->sync($data['artists']);
        $release->companies()->sync($data['companies']);
        $release->genres()->sync($data['genres']);

        return response()->json($release);
    }

    public function activate(Request $request)
    {
        $data = $request->all();
        $release = Release::find($data['id']);
        $release->is_draft = false;
        $release->save();

        try {
            $releaseMail = DB::table('site_sent_releases')->where('release_id', $data['id'])->first();
            Mail::to($releaseMail->mail_sent_by)->send(new ActivateRelease($release));
        } catch (Throwable $e) {
            return false;
        }
    }

    public function destroy(Request $request)
    {
        $release = Release::find($request['release']);
        $release->delete();
    }
}
