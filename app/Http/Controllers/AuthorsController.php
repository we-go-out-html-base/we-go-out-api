<?php
namespace App\Http\Controllers;

use App\Http\Requests\StoreAuthorRequest;
use App\Http\Requests\UpdateAuthorRequest;
use App\Models\Author;
use App\Models\RoleAuthor;
use Illuminate\Http\Request;
use Inertia\Inertia;

class AuthorsController extends Controller
{
    public function index()
    {
        return Inertia::render('Author/List');
    }

    public function edit(Request $request)
    {
        $author = Author::with(['social_media'])->findOrFail($request['author']);
        return Inertia::render('Author/Edit', [
            'author' => $author,
            'positions' => RoleAuthor::get()->pluck('name')->toArray()
        ]);
    }

    public function create(Request $request)
    {
        return Inertia::render(
            'Author/Create',
            [
                'positions' => RoleAuthor::get()->pluck('name')->toArray()
            ]
        );
    }

    public function update(UpdateAuthorRequest $request)
    {
        $data = $request->all();
        $author = Author::find($request['author']);
        $author->update($data);
        $author->social_media()->sync($data['social_media']);
    }

    public function store(StoreAuthorRequest $request)
    {
        $data = $request->all();
        $author = Author::create($data);
        $author->social_media()->sync($data['social_media']);

        return response()->json($author);
    }

    public function getPositions()
    {
        return RoleAuthor::get()->pluck('name')->toArray();
    }

    public function destroy(Request $request)
    {
        $author = Author::find($request['author']);
        $author->delete();
    }

}
