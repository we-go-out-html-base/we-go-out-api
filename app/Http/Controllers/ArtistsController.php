<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Artist;
use App\Models\Genre;
use App\Models\Country;
use App\Models\Company;
use App\Models\News;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;
use App\Helpers\AddVideoEmbedTags;
use App\Helpers\HandleSlugString;
use App\Helpers\RemoveDiacritic;
use App\Http\Requests\UpdateArtistRequest;
use App\Http\Requests\StoreArtistRequest;

class ArtistsController extends Controller
{
    public function index()
    {
        return Inertia::render('Artist/List');
    }

    public function edit(Request $request)
    {
        $artist = Artist::with(['social_media', 'news', 'genres', 'country', 'companies'])->findOrFail($request['artist']);
        return Inertia::render('Artist/Edit', [
            'artist' => $artist,
            'genres' => Genre::all(),
            'countries' => Country::all(),
            'companies' => Company::all()
        ]);
    }

    public function create(Request $request)
    {
        return Inertia::render(
            'Artist/Create',
            [
                'genres' => Genre::all(),
                'countries' => Country::all(),
                'companies' => Company::all()
            ]
        );
    }

    public function update(UpdateArtistRequest $request)
    {
        $data = $request->all();

        $verificaslug = Artist::where('slug', $data['slug'])->where('id', '<>', $data['id'])->count();

        if ($verificaslug > 0) {
            return response()->json(['error' => 'O slug já está sendo usado por outro conteúdo'], 422);
        }

        $data['slug'] = HandleSlugString::handle($data['slug']);
        $data['slug'] = RemoveDiacritic::remove($data['slug']);
        array_key_exists('biography', $data) && $data['biography'] = AddVideoEmbedTags::addTags($data['biography']);

        // dd(array_keys($data));
        $artist = Artist::find($request['artist']);
        $artist->update($data);
        $artist->genres()->sync($data['genres']);
        $artist->social_media()->sync([]); // linha add
        $artist->social_media()->sync($data['social_media']);
        $artist->companies()->sync($data['companies']);

        return response()->json(['message' => 'Evento atualizado com sucesso']);
    }

    public function store(StoreArtistRequest $request)
    {
        $data = $request->all();

        $data['slug'] = HandleSlugString::handle($data['slug']);
        $data['slug'] = RemoveDiacritic::remove($data['slug']);
        array_key_exists('biography', $data) && $data['biography'] = AddVideoEmbedTags::addTags($data['biography']);

        $artist = Artist::create($data);
        $artist->genres()->sync($data['genres']);
        $artist->social_media()->sync($data['social_media']);
        $artist->companies()->sync($data['companies']);
        return response()->json($artist);
    }

    public function destroy(Request $request)
    {
        $artist = Artist::find($request['artist']);
        $artist->delete();
    }
}
