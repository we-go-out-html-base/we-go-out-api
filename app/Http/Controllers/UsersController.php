<?php

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;

class UsersController extends Controller
{
    public function index()
    {
        return Inertia::render('User/List');
    }

    public function edit(Request $request)
    {
        $user = User::with(['role'])->findOrFail($request['user']);
        return Inertia::render('User/Edit', [
            'user' => $user,
        ]);
    }

    public function create(Request $request)
    {
        return Inertia::render('User/Create');
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $user = User::find($request['user']);

        $data = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . $user->id,
            'password' => 'nullable',
            'role_id' => 'nullable'
        ]);

        if(isset($data['password']) && !empty($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        } else {
            unset($data['password']);
        }

        $user->update($data);
        // if($data['password'] !== null)
        // {
        //     $user->update(['password' => Hash::make($data['password'])]);
        // }        
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $data = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required',
            'password' => 'nullable',
            'role_id' => 'nullable'
        ]);
        $user = User::create($data);
        $user->update(['password' => Hash::make($data['password'])]);
    }




}
