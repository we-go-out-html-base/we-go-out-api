<?php
namespace App\Http\Controllers;

use App\Http\Requests\StateRequest;
use App\Models\State;
use App\Models\Country;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;

class StatesController extends Controller
{

    public function index()
    {
        return Inertia::render('State/List');
    }

    public function edit(Request $request)
    {
        $state = State::findOrFail($request['state']);
        return Inertia::render('State/Edit', [
            'state' => $state,
            'countriesList' => Country::all()
        ]);
    }

    public function update(StateRequest $request)
    {
        $data = $request->all();
        $highlight = State::find($request['state']);
        $highlight->update($data);
    }

    public function create()
    {
        return Inertia::render('State/Create');
    }

    public function store(StateRequest $request)
    {
        $data = $request->all();
        $state = State::create($data);
    }

    public function destroy(Request $request)
    {
        $state = State::find($request['state']);
        $state->delete();
    }




}
