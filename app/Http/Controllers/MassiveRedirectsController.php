<?php

namespace App\Http\Controllers;


use App\Models\Event;
use App\Models\Artist;
use App\Models\Company;
use App\Models\MassiveRedirect;
use App\Models\Release;
use App\Models\TypeRelease;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Mail\ActivateRelease;
use Illuminate\Support\Facades\DB;

class MassiveRedirectsController extends Controller
{
    public function index()
    {
        return Inertia::render('MassiveRedirect/List');
    }

    public function edit(Request $request)
    {
        $redirect = MassiveRedirect::findOrFail($request['redirect']);
        return Inertia::render('MassiveRedirect/Edit', [
            'redirect' => $redirect,
        ]);
    }

    public function create(Request $request)
    {
        return Inertia::render('MassiveRedirect/Create', [

        ]);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        

    
        $redirect = MassiveRedirect::find($request['id']);
        $redirect->update($data);

    
        return response()->json(['message' => 'Redirect atualizado com sucesso']);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $redirect = MassiveRedirect::create($data);

        return response()->json($redirect);
    }



    public function destroy(Request $request)
    {
        $redirect = MassiveRedirect::find($request['redirect']);
        $redirect->delete();
    }
}