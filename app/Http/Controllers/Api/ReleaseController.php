<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Release;
use Illuminate\Http\Request;
use App\Http\Requests\Api\CreateReleaseRequest;
use App\Mail\SubmittedRelease;
use App\Models\SiteSender;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class ReleaseController extends Controller
{
    public function index(Request $request)
    {
        $data = $request->all();
        $perPage = isset($data['perPage']) ? $data['perPage'] : 12;
        if(isset($data['slug'])){
            $releases = Release::with(['artists', 'companies', 'genres', 'type'])
            ->where('slug', $data['slug'])
            ->limit(1)
            ->paginate(1);
            return response($releases, 200);
        }

        $sortedBy = isset($data['sortedBy']) ? $data['sortedBy'] : 'name';
        $sorted = isset($data['sorted']) ? $data['sorted'] : 'ASC';

        $query = Release::with(['artists', 'companies', 'genres', 'type'])
        ->when(isset($data['id']), function ($query) use($data) {
            $query->whereIn('id', explode(",", $data['id']));
        })
        ->when(isset($data['name']) && !isset($data['adminFilters']), function ($query) use($data) {
                $query->where('name', 'like', '%' . $data['name'] . '%');
        })
        ->when(isset($data['slug']), function ($query) use($data) {
            $query->where('slug', 'like', '%' . $data['slug'] . '%');
        })
        ->when(isset($data['date']), function ($query) use($data) {
            $query->whereDate('date', $data['date']);
        })
        ->when(isset($data['month']), function ($query) use($data) {
            $query->whereMonth('date', date('m', date(strtotime($data['month']))))->whereYear('date', date('Y', date(strtotime($data['month']))));
        })
        ->when(isset($data['artist_id']), function ($query) use($data) {
            $query->whereHas('artists', function ($query) use ($data) {
                $query->whereIn('artist_id', explode("," ,$data['artist_id']));
            });
        })
        ->when(isset($data['type_id']), function ($query) use($data) {
            $query->whereIn('type_id', explode(",", $data['type_id']));
        })
        ->when(isset($data['company_id']), function ($query) use($data) {
            $query->whereHas('companies', function ($query) use ($data) {
                $query->whereIn('company_id', explode("," ,$data['company_id']));
            });
        })
        ->when(!isset($data['adminFilters']), function ($query) use($data) {
            $query->where('is_draft', 0)->where('date', "<=", Carbon::now());

        })
        ->when(isset($data['adminFilters']), function ($query) use($data) {
            if(isset($data['name'])){
                $query->whereHas('artists', function ($query) use($data)  {
                    $query->where('name', 'like', '%' . $data['name'] . '%');
                })->orWhere('name', 'like', '%' . $data['name'] . '%');
            }

            // aqui especialmente pros filtros do painel admin
            if($data['adminFilters'] == 'publicados'){
                $query->where('is_draft', 0)->where('date', "<=", Carbon::now());

            }
            if($data['adminFilters'] == 'excluidos'){
                $query->where('is_draft', 0)->where('date', "<=", Carbon::now());
                $query->onlyTrashed();
            }
            if($data['adminFilters'] == 'rascunhos'){
                $query->where('is_draft', 1);
                $query->where('created_by', '<>', 'site');
            }
            if($data['adminFilters'] == 'enviados pelo site'){
                $query->where('is_draft', 1);
                $query->where('created_by', 'site');
            }
            if($data['adminFilters'] == 'agendados'){
                 $query->where('is_draft', 0);
                $query->where('date', ">", Carbon::now()->subHours(3));
            }

        })
        ->when(isset($data['genre_id']), function ($query) use($data) {
            $query->whereHas('genres', function ($query) use ($data) {
                $query->whereIn('genre_id', explode("," ,$data['genre_id']));
            });
            if (!$query->exists()) {
                return Release::where('id', '<', 0);
            }
        })

        ->when(isset($data['is_main']), function ($query) use($data) {
            $query->where('is_main', $data['is_main']);
        });
        $query->orderBy($sortedBy, $sorted);
        $query->orderBy('name', 'ASC');

       $releases = $query->paginate($perPage);

        return response($releases, 200);
    }

    public function create(CreateReleaseRequest $request)
    {
        $data = $request->all();

        try {
            $release = Release::create($data);
            isset($request->artists) && $release->artists()->sync(explode(",", $request->artists));
            isset($request->companies) && $release->companies()->sync(explode(",", $request->companies));
            isset($request->genres) && $release->genres()->sync(explode(",", $request->genres));

            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $imageName = $image->getClientOriginalName();
                $image->move(public_path('images/releases/' . $release->id . '/'), $imageName);
                $release->image = "https://" .$_SERVER['HTTP_HOST'] . '/images/releases/' . $release->id . '/' . $imageName;
                $release->update();
            }

            return response()->json($release, 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function cachedReleasesList()
    {
        if(!Cache::get('releases')){
            $artists = DB::table('releases')->select('name', 'slug')->where('is_draft', 0)->where('deleted_at', null)->get();
            Cache::put('releases', $artists, 86400);
        }
        return Cache::get('releases');
    }

}

function str_replace_array($search, array $replace, $subject)
{
    foreach ($replace as $value) {
        $subject = preg_replace('/\?/', is_numeric($value) ? $value : "'{$value}'", $subject, 1);
    }
    return $subject;
}
