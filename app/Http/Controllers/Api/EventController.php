<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\EventCreateRequest;
use App\Models\Event;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Http\Requests\Api\CreateEventRequest;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\SubmittedEvent;
// use App\Models\SiteSender;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

/**
 * @OA\Info(
 *     title="WGO API",
 *     version="0.1"
 * )
 */
class EventController extends Controller
{
    public function index(Request $request)
    {
        $data = $request->all();

        $sortedBy = isset($data['sortedBy']) ? $data['sortedBy'] : 'start_date';
        $sorted = isset($data['sorted']) ? $data['sorted'] : 'ASC';
        $perPage = isset($data['perPage']) ? $data['perPage'] : 12;

        if (isset($data['slug'])) {
            $events = Event::with(['eventType', 'location', 'questions', 'artists', 'artists.genres', 'location.city', 'location.city.state', 'location.city.state.country', 'togethers', 'linkedEvents', 'news', 'review'])
                ->where('slug', $data['slug'])
                ->limit(1)
                ->paginate(1);
            return response($events, 200);
        }

        $events = Event::with(['eventType', 'location', 'questions', 'artists', 'artists.genres', 'location.city', 'location.city.state', 'location.city.state.country', 'togethers', 'linkedEvents', 'news', 'review'])
            ->when(isset($data['id']), function ($query) use ($data) {
                $query->whereIn('id', explode(",", $data['id']));
            })
            ->when(isset($data['name']), function ($query) use ($data) {
                $eventsIdsByName = Event::where('name', 'like', '%' . $data['name'] . '%')
                    ->orWhere('search_terms', 'like', '%' . $data['name'] . '%')->pluck('id')->toArray();
                $query->whereIn('id', $eventsIdsByName);
            })
            ->when(isset($data['start_date']) && isset($data['end_date']), function ($query) use ($data) {
                $startDate = Carbon::create($data['start_date']);
                $endDate = Carbon::create($data['end_date'])->setTime(23, 59, 59);
                $query->where(function ($query) use ($data, $startDate, $endDate) {
                    $query->whereBetween('start_date', [$startDate, $endDate])
                        ->orWhereBetween('end_date', [$startDate, $endDate])
                        ->orWhere(function ($query) use ($data, $startDate, $endDate) {
                            $query->where('start_date', '<=', $startDate)
                                ->where('end_date', '>=', $startDate);
                        });
                });
            })
            ->when(isset($data['slug']), function ($query) use ($data) {
                $query->where('slug', 'like', '%' . $data['slug'] . '%')->first();
            })
            ->when(isset($data['date']), function ($query) use ($data) {
                $query->whereDate('start_date', '<=', $data['date'])->whereDate('end_date', '>=', $data['date']);
            })
            ->when(isset($data['location_id']), function ($query) use ($data) {
                $query->whereIn('location_id', explode(",", $data['location_id']));
            })
            ->when(isset($data['event_type_id']), function ($query) use ($data) {
                $query->whereIn('event_type_id', explode(",", $data['event_type_id']));
            })
            ->when(isset($data['genre_id']), function ($query) use ($data) {
                $query->whereHas('artists', function ($query) use ($data) {
                    $query->whereHas('genres', function ($query) use ($data) {
                        $query->whereIn('genre_id', explode(",", $data['genre_id']));
                    });
                });
            })
            ->when(isset($data['forMaps']), function ($query) use ($data) {
                $query->whereHas('location', function ($query) use ($data) {
                    $query->where('latitude', '<>', null);
                    $query->whereNotNull('longitude');
                });
            })
            ->when(isset($data['artist_id']), function ($query) use ($data) {
                $query->whereHas('artists', function ($query) use ($data) {
                    $query->whereIn('artist_id', explode(",", $data['artist_id']));
                });
            })
            ->when(isset($data['is_main']), function ($query) use ($data) {
                $query->where('is_main', $data['is_main']);
            })
            ->when(isset($data['city_id']), function ($query) use ($data) {
                $query->whereHas('location', function ($query) use ($data) {
                    $query->whereIn('city_id', explode(",", $data['city_id']));
                });
            })
            ->when(isset($data['state_id']), function ($query) use ($data) {
                $query->whereHas('location', function ($query) use ($data) {
                    $query->whereHas('city', function ($query) use ($data) {
                        $query->whereIn('state_id', explode(",", $data['state_id']));
                    });
                });
            })
            ->when(isset($data['country_id']), function ($query) use ($data) {
                $query->whereHas('location', function ($query) use ($data) {
                    $query->whereHas('city', function ($query) use ($data) {
                        $query->whereHas('state', function ($query) use ($data) {
                            $query->where('country_id', explode(",", $data['country_id']));
                        });
                    });
                });
            })
            ->when(isset($data['sw']) && isset($data['ne']), function ($query) use ($data) {
                $sw = explode(',', $data['sw']);
                $ne = explode(',', $data['ne']);

                if (count($sw) == 2 && count($ne) == 2) {
                    $swLat = $sw[0];
                    $swLng = $sw[1];
                    $neLat = $ne[0];
                    $neLng = $ne[1];

                    $minLat = min($swLat, $neLat);
                    $maxLat = max($swLat, $neLat);
                    $minLng = min($swLng, $neLng);
                    $maxLng = max($swLng, $neLng);

                    $query->whereHas('location', function ($query) use ($minLat, $maxLat, $minLng, $maxLng) {
                        $query->whereBetween('latitude', [$minLat, $maxLat])
                          ->whereBetween('longitude', [$minLng, $maxLng]);
                    });
                }
            })
            ->when(!isset($data['adminFilters']), function ($query) use ($data) {
                $query->where('is_draft', 0);
                $query->where('publish_date', '<=', Carbon::now()->subHours(3));
            })
            ->when(isset($data['adminFilters']), function ($query) use ($data) {
                // aqui especialmente pros filtros do painel admin
                if ($data['adminFilters'] == 'publicados') {
                    $query->where('is_draft', 0)->where('publish_date', "<=", Carbon::now()->subHours(3));
                }
                if ($data['adminFilters'] == 'excluidos') {
                    $query->where('is_draft', 0)->where('publish_date', "<=", Carbon::now()->subHours(3));
                    $query->onlyTrashed();
                }
                if ($data['adminFilters'] == 'rascunhos') {
                    $query->where('is_draft', 1);
                    $query->where('created_by', '<>', 'site');
                }
                if ($data['adminFilters'] == 'enviados pelo site') {
                    $query->where('is_draft', 1);
                    $query->where('created_by', 'site');
                }
                if ($data['adminFilters'] == 'agendados') {
                    $query->where('is_draft', 0);
                    $query->where('publish_date', ">=", Carbon::now()->subHours(3));
                }
            })
            ->when(isset($data['future']), function ($query) use ($data) {
                $query->where('end_date', '>', Carbon::now())->orderBy('start_date', 'ASC');
            })
            ->orderBy($sortedBy, $sorted)
            ->orderBy('name', 'ASC')
            ->paginate($perPage);

        return response($events, 200);
    }

    public function create(EventCreateRequest $request)
    {
        $data = $request->all();
        try {
            $event = Event::create($data);
            isset($request->artists) && $event->artists()->sync(explode(",", $request->artists));

            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $imageName = $image->getClientOriginalName();
                $image->move(public_path('images/events/' . $event->id . '/'), $imageName);
                $event->image = "https://" . $_SERVER['HTTP_HOST'] . '/images/events/' . $event->id . '/' . $imageName;
                $event->update();
            }

            return response()->json($event, 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function cachedEventsList()
    {
        if (!Cache::get('events')) {
            $events = DB::table('events')->select('name', 'slug')->where('is_draft', 0)->where('publish_date', '<=', Carbon::now())->where('deleted_at', null)->get();
            Cache::put('events', $events, 86400);
        }
        return Cache::get('events');
    }

    public function eventsForMap(Request $request)
    {
        $data = $request->all();

        $sortedBy = isset($data['sortedBy']) ? $data['sortedBy'] : 'start_date';
        $sorted = isset($data['sorted']) ? $data['sorted'] : 'ASC';

        $query = Event::with(['location', 'eventType', 'artists', 'location.city', 'location.city.state'])
            ->whereHas('location', function ($query) {
                $query->whereNotNull('latitude');
            })
            ->when(isset($data['sw']) && isset($data['ne']), function ($query) use ($data) {
                $sw = explode(',', $data['sw']);
                $ne = explode(',', $data['ne']);

                if (count($sw) == 2 && count($ne) == 2) {
                    // $swLat = (int) floor($sw[0]);
                    // $swLng = (int) floor($sw[1]);
                    // $neLat = (int) floor($ne[0]);
                    // $neLng = (int) floor($ne[1]);

                    $swLat = $sw[0];
                    $swLng = $sw[1];
                    $neLat = $ne[0];
                    $neLng = $ne[1];

                    $minLat = min($swLat, $neLat);
                    $maxLat = max($swLat, $neLat);
                    $minLng = min($swLng, $neLng);
                    $maxLng = max($swLng, $neLng);

                    $query->whereHas('location', function ($query) use ($minLat, $maxLat, $minLng, $maxLng) {
                        $query->whereBetween('latitude', [$minLat, $maxLat])
                          ->whereBetween('longitude', [$minLng, $maxLng]);
                    });

                    // Log::info('Locations:', [
                    //     'Locations' => Location::whereBetween('latitude', [$swLat, $neLat])
                    //                            ->whereBetween('longitude', [$swLng, $neLng])
                    //                            ->get()
                    // ]);
                }
            }, function ($query) use ($data) {
                if (isset($data['location_id'])) {
                    $query->where('location_id', $data['location_id']);
                }
            })
            ->when(isset($data['name']), function ($query) use ($data) {
                $query->where('name', 'like', '%' . $data['name'] . '%');
            })
            ->when(isset($data['date']), function ($query) use ($data) {
                $query->whereDate('start_date', '<=', $data['date'])->whereDate('end_date', '>=', $data['date']);
            })
            ->when(isset($data['start_date']) && isset($data['end_date']), function ($query) use ($data) {
                $query->where(function ($query) use ($data) {
                    $query->whereBetween('start_date', [$data['start_date'], $data['end_date']]);
                });
            })
            ->when(isset($data['artist_id']), function ($query) use ($data) {
                $query->whereHas('artists', function ($query) use ($data) {
                    $query->whereIn('artist_id', explode(",", $data['artist_id']));
                });
            })
            ->when(isset($data['city_id']), function ($query) use ($data) {
                $query->whereHas('location', function ($query) use ($data) {
                    $query->whereIn('city_id', explode(",", $data['city_id']));
                });
            })
            ->when(isset($data['event_type_id']), function ($query) use ($data) {
                $query->whereIn('event_type_id', explode(",", $data['event_type_id']));
            })
            ->when(isset($data['future']), function ($query) use ($data) {
                $query->where('end_date', '>', Carbon::now())->orderBy('start_date', 'ASC');
            });

        // $sql = str_replace_array('?', $query->getBindings(), $query->toSql());
        // Log::info('Generated SQL:', ['sql' => $sql]);
        $query->orderBy($sortedBy, $sorted);
        $events = $query->get();

        return $events;
    }
}

function str_replace_array($search, array $replace, $subject)
{
    foreach ($replace as $value) {
        $subject = preg_replace('/\?/', is_numeric($value) ? $value : "'{$value}'", $subject, 1);
    }
    return $subject;
}
