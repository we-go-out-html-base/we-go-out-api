<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Artist;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class ArtistController extends Controller
{

    /**
     * @OA\Get(
     *     tags={"Artists"},
     *     path="/api/artists",
     *     summary="Retrieve artists",
     *     @OA\Response(
     *         response="200",
     *         description="The data"
     *     ),
     *      @OA\Parameter(
     *         in="query",
     *         name="id",
     *         required=false,
     *         example="1,3,55",
     *         @OA\Schema(
     *             type="string",
     *          )
     *       ),
     *      @OA\Parameter(
     *         in="query",
     *         name="name",
     *         required=false,
     *         example="art",
     *       ),
     *      @OA\Parameter(
     *         in="query",
     *         name="slug",
     *         required=false,
     *         example="art",
     *         @OA\Schema(
     *             type="string"
     *          )
     *       ),
     *      @OA\Parameter(
     *         in="query",
     *         name="genre_id",
     *         required=false,
     *         example="1,3,55",
     *         @OA\Schema(
     *             type="string",
     *          )
     *       ),
     *      @OA\Parameter(
     *         in="query",
     *         name="country_id",
     *         required=false,
     *         example="1,3,55",
     *         @OA\Schema(
     *             type="string",
     *          )
     *       ),
     *      @OA\Parameter(
     *         in="query",
     *         name="company_id",
     *         required=false,
     *         example="1,3,55",
     *         @OA\Schema(
     *             type="string",
     *          )
     *       ),
     *      @OA\Parameter(
     *         in="query",
     *         name="is_main",
     *         required=false,
     *         example="bool",
     *         @OA\Schema(
     *             type="boolean",
     *          )
     *       ),
     * )
     */
    public function index(Request $request)
    {
        $data = $request->all();


        if (isset($data['slug'])) {
            $artists = Artist::with(['news', 'genres', 'country', 'companies', 'social_media'])
                ->where('slug', $data['slug'])
                ->limit(1)
                ->paginate(1);
            return response($artists, 200);
        }

        $sortedBy = isset($data['sortedBy']) ? $data['sortedBy'] : 'name';
        $sorted = isset($data['sorted']) ? $data['sorted'] : 'ASC';
        $perPage = isset($data['perPage']) ? $data['perPage'] : 12;

        $artists = Artist::with(['news', 'genres', 'country', 'companies', 'social_media'])
            ->when(isset($data['id']), function ($query) use ($data) {
                $query->whereIn('id', explode(",", $data['id']));
            })
            ->when(isset($data['name']), function ($query) use ($data) {
                $query->where('name', 'like', '%' . $data['name'] . '%');
            })
            ->when(isset($data['slug']), function ($query) use ($data) {
                $query->where('slug', 'like', '%' . $data['slug'] . '%');
            })
            ->when(isset($data['genre_id']), function ($query) use ($data) {
                $query->whereHas('genres', function ($query) use ($data) {
                    $query->whereIn('genre_id', explode(",", $data['genre_id']));
                });
            })
            ->when(isset($data['country_id']), function ($query) use ($data) {
                $query->whereIn('country_id', explode(",", $data['country_id']));
            })
            ->when(isset($data['company_id']), function ($query) use ($data) {
                $query->whereHas('companies', function ($query) use ($data) {
                    $query->whereIn('company_id', explode(",", $data['company_id']));
                });
            })
            ->when(isset($data['is_main']), function ($query) use ($data) {
                $query->where('is_main', $data['is_main']);
            })
            ->when(!isset($data['adminFilters']), function ($query) use ($data) {
                $query->where('is_draft', 0);

            })
            ->when(isset($data['adminFilters']), function ($query) use ($data) {
                // aqui especialmente pros filtros do painel admin
                if ($data['adminFilters'] == 'publicados') {
                    $query->where('is_draft', 0);
                }
                if ($data['adminFilters'] == 'excluidos') {
                    $query->onlyTrashed();
                }
                if ($data['adminFilters'] == 'rascunhos') {
                    $query->where('is_draft', 1);
                }
            })
            ->orderBy($sortedBy, $sorted)
            ->paginate($perPage);
        return response($artists, 200);
    }

    public function cachedArtistsList()
    {
        if(!Cache::get('artists')){
            $artists = DB::table('artists')->select('id', 'name', 'slug')->where('is_draft', 0)->where('deleted_at', null)->get();
            Cache::put('artists', $artists, 86400);
        }
        return Cache::get('artists');
    }

}
