<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;

class PagesController extends Controller
{

    /**
     * @OA\Get(
     *     tags={"Pages"},
     *     path="/api/pages",
     *     summary="Retrieve pages",
     *     @OA\Response(
     *         response="200",
     *         description="The data"
     *     ),
     *      @OA\Parameter(
     *         in="query",
     *         name="id",
     *         required=false,
     *         example="1,3,55",
     *         @OA\Schema(
     *             type="string",
     *          )
     *       ),
     *      @OA\Parameter(
     *         in="query",
     *         name="title",
     *         required=false,
     *         example="art",
     *       ),
     *      @OA\Parameter(
     *         in="query",
     *         name="sub_title",
     *         required=false,
     *         example="art",
     *       ),
     *      @OA\Parameter(
     *         in="query",
     *         name="content",
     *         required=false,
     *         example="art",
     *       ),
     *      @OA\Parameter(
     *         in="query",
     *         name="slug",
     *         required=false,
     *         example="art",
     *       ), 
     *      @OA\Parameter(
     *         in="query",
     *         name="key_words",
     *         required=false,
     *         example="content",
     *       ),
     * )
     */
    public function index(Request $request)
    {
        $data = $request->all();

        $pages = Page::with([])
        ->when(isset($data['id']), function ($query) use($data) {
            $query->whereIn('id', explode(",", $data['id']));
        })
        ->when(isset($data['title']), function ($query) use($data) {
            $query->where('title', 'like', '%' . $data['title'] . '%');
        })
        ->when(isset($data['sub_title']), function ($query) use($data) {
            $query->where('sub_title', 'like', '%' . $data['sub_title'] . '%');
        })
        ->when(isset($data['content']), function ($query) use($data) {
            $query->where('content', 'like', '%' . $data['content'] . '%');
        })
        ->when(isset($data['slug']), function ($query) use($data) {
            $query->where('slug', 'like', '%' . $data['slug'] . '%');
        })
        ->when(isset($data['key_words']), function ($query) use($data) {
            $query->where('key_words', 'like', '%' . $data['key_words'] . '%');
        })
        ->get();
        return response($pages, 200);
    }

}
