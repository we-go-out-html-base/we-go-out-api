<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\News;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{

    /**
     * @OA\Get(
     *     tags={"News"},
     *     path="/api/news",
     *     summary="Retrieve news",
     *     @OA\Response(
     *         response="200",
     *         description="The data"
     *     ),
     *      @OA\Parameter(
     *         in="query",
     *         name="id",
     *         required=false,
     *         example="1,3,55",
     *         @OA\Schema(
     *             type="string",
     *          )
     *       ),
     *      @OA\Parameter(
     *         in="query",
     *         name="author_id",
     *         required=false,
     *         example="1,3,55",
     *         @OA\Schema(
     *             type="string",
     *          )
     *       ),
     *      @OA\Parameter(
     *         in="query",
     *         name="title",
     *         required=false,
     *         example="art",
     *       ),
     *      @OA\Parameter(
     *         in="query",
     *         name="slug",
     *         required=false,
     *         example="art",
     *       ), 
     *      @OA\Parameter(
     *         in="query",
     *         name="content",
     *         required=false,
     *         example="content",
     *       ),
     *      @OA\Parameter(
     *         in="query",
     *         name="category_id",
     *         required=false,
     *         example="1,3,55",
     *         @OA\Schema(
     *             type="string",
     *          )
     *       ),
     *      @OA\Parameter(
     *         in="query",
     *         name="artist_id",
     *         required=false,
     *         example="1,3,55",
     *         @OA\Schema(
     *             type="string",
     *          )
     *       ),
     * )
     */
    public function index(Request $request)
    {
        $data = $request->all();

        $sortedBy = isset($data['sortedBy']) ? $data['sortedBy'] : 'id';
        $sorted = isset($data['sorted']) ? $data['sorted'] : 'DESC';
        $perPage = isset($data['perPage']) ? $data['perPage'] : 12;

        if (isset($data['slug'])) {
            $news = News::with(['artists', 'author', 'author.social_media', 'category', 'events'])
                ->where('slug', $data['slug'])
                ->limit(1)
                ->paginate(1);
            return response($news, 200);
        }

        $news = News::with(['artists', 'author', 'author.social_media', 'category', 'events'])
            ->when(isset($data['id']), function ($query) use ($data) {
                $query->whereIn('id', explode(",", $data['id']));
            })
            ->when(isset($data['author_id']), function ($query) use ($data) {
                $query->whereIn('author_id', explode(",", $data['author_id']));
            })
            ->when(function ($query) use ($data) {
                if (isset($data['name'])) {
                    $query->where(function ($query) use ($data) {
                        $query->where('title', 'like', '%' . $data['name'] . '%')
                            ->orWhere('search_terms', 'like', '%' . $data['name'] . '%');
                    });
                }
            })
            // ->when(isset($data['name']), function ($query) use ($data) {
            //     $query->where('title', 'like', '%' . $data['name'] . '%')
            //     ->orWhere('search_terms', 'like', '%' . $data['name'] . '%')->pluck('id')->toArray();
            // })
            ->when(isset($data['title']), function ($query) use ($data) {
                $query->where('title', 'like', '%' . $data['title'] . '%');
            })
            ->when(isset($data['slug']), function ($query) use ($data) {
                $query->where('slug', 'like', '%' . $data['slug'] . '%');
            })
            ->when(isset($data['content']), function ($query) use ($data) {
                $query->where('content', 'like', '%' . $data['content'] . '%');
            })
            ->when(isset($data['category_id']), function ($query) use ($data) {
                $query->where('category_id', $data['category_id']);
            })
            ->when(isset($data['event_id']), function ($query) use ($data) {
                $query->whereHas('events', function ($query) use ($data) {
                    $query->whereIn('event_id', explode(",", $data['event_id']));
                });
            })
            ->when(isset($data['is_main']), function ($query) use ($data) {
                $query->where('is_main', $data['is_main']);
            })
            ->when(isset($data['artist_id']), function ($query) use ($data) {
                $query->whereHas('artists', function ($query) use ($data) {
                    $query->whereIn('artist_id', explode(",", $data['artist_id']));
                });
            })
            ->when(!isset($data['adminFilters']), function ($query) use ($data) {
                $query->where('is_draft', 0);
                $query->where('public_date', '<=', Carbon::now()->subHours(3));
            })
            ->when(isset($data['adminFilters']), function ($query) use ($data) {
                // aqui especialmente pros filtros do painel admin
                if ($data['adminFilters'] == 'publicados') {
                    $query->where('is_draft', 0);
                    $query->where('public_date', '<=', Carbon::now()->subHours(3));
                }
                if ($data['adminFilters'] == 'excluidos') {
                    $query->onlyTrashed();
                }
                if ($data['adminFilters'] == 'rascunhos') {
                    $query->where('is_draft', 1);
                }
                if ($data['adminFilters'] == 'agendados') {
                    $query->where('is_draft', 0);
                    $query->where('public_date', '>=', Carbon::now()->subHours(3));
                }
            })
            ->orderBy($sortedBy, $sorted)
            //->orderBy('public_date', $sorted)
            ->paginate($perPage);

        return response($news, 200);
    }

    public function cachedListNews()
    {
        if (!Cache::get('news')) {
            $news = DB::table('news')->select('title', 'slug')->where('status', 1)->where('is_draft', 0)->where('public_date', '<=', Carbon::now())->where('deleted_at', null)->get();
            Cache::put('news', $news, 86400);
        }
        return Cache::get('news');
    }
}
