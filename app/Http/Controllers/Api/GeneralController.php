<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\Genre;
use App\Models\Location;
use App\Models\Company;
use App\Models\Category;
use App\Models\EventType;
use App\Models\Tag;
use App\Models\Highlight;
use App\Models\Ad;
use App\Models\User;
use App\Models\Author;
use App\Models\AdCategory;
use App\Models\HighlightType;
use App\Models\Together;
use App\Models\TogetherCategory;
use App\Models\SocialMedia;
use App\Models\LinkTree;
use App\Models\RoleUser;
use App\Models\MassiveRedirect;
use App\Models\SiteData;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class GeneralController extends Controller
{

  /**
   * @OA\Get(
   *     tags={"General"},
   *     path="/api/countries",
   *     summary="Retrieve countries",
   *     @OA\Response(
   *         response="200",
   *         description="The data"
   *     ),
   *     @OA\Parameter(
   *         in="query",
   *         name="name",
   *         required=false,
   *         example="1",
   *         @OA\Schema(
   *             type="string"
   *          )
   *       ),

   * )
   */
  public function listCountries(Request $request)
  {
    $data = $request->all();
    $countries = Country::when(isset($data['name']), function ($query) use ($data) {
      $query->where('name', 'like', '%' . $data['name'] . '%');
    })
      ->get();
    return response($countries, 200);
  }


  /**
   * @OA\Get(
   *     tags={"General"},
   *     path="/api/states",
   *     summary="Retrieve states",
   *     @OA\Response(
   *         response="200",
   *         description="The data"
   *     ),
   *     @OA\Parameter(
   *         in="query",
   *         name="country_id",
   *         required=false,
   *         example="1",
   *         @OA\Schema(
   *             type="int"
   *          )
   *       ),
   *     @OA\Parameter(
   *         in="query",
   *         name="name",
   *         required=false,
   *         example="nome",
   *         @OA\Schema(
   *             type="string"
   *          )
   *       ),
   * )
   */
  public function listStates(Request $request)
  {
    $data = $request->all();
    $states = State::with('country', 'cities')->when(isset($data['country_id']), function ($query) use ($data) {
      $query->where('country_id', $data['country_id']);
    })
      ->when(isset($data['name']), function ($query) use ($data) {
        $query->where('name', 'like', '%' . $data['name'] . '%');
      })
      ->get();
    return response($states, 200);
  }

  public function citiesForMap()
  {
      $cities = City::with('state', 'state.country')->whereIn('id', Location::select('city_id')->get()->toArray())->get();
      return response($cities, 200);
  }


  /**
   * @OA\Get(
   *     tags={"General"},
   *     path="/api/cities",
   *     summary="Retrieve cities",
   *     @OA\Response(
   *         response="200",
   *         description="The data"
   *     ),
   *     @OA\Parameter(
   *         in="query",
   *         name="state_id",
   *         required=false,
   *         example="1",
   *         @OA\Schema(
   *             type="int"
   *          )
   *       ),
   *     @OA\Parameter(
   *         in="query",
   *         name="name",
   *         required=false,
   *         example="nome",
   *         @OA\Schema(
   *             type="string"
   *          )
   *       ),
   * )
   */
  public function listCities(Request $request)
  {
    $data = $request->all();

    if (isset($data['no_pagination'])) {
      $cities = City::when(isset($data['state_id']), function ($query) use ($data) {
        $query->where('state_id', $data['state_id']);
      })
        ->when(isset($data['name']), function ($query) use ($data) {
          $query->where('name', 'like', '%' . $data['name'] . '%');
        })
        ->get();
    } else {
      $cities = City::with('state', 'state.country')->when(isset($data['state_id']), function ($query) use ($data) {
        $query->where('state_id', $data['state_id']);
      })
        ->when(isset($data['name']), function ($query) use ($data) {
          $query->where('name', 'like', '%' . $data['name'] . '%');
        })
        ->paginate(10);

    }

    return response($cities, 200);
  }

  /**
   * @OA\Get(
   *     tags={"General"},
   *     path="/api/genres",
   *     summary="Retrieve genres",
   *     @OA\Response(
   *         response="200",
   *         description="The data"
   *     ),
   *     @OA\Parameter(
   *         in="query",
   *         name="name",
   *         required=false,
   *         example="nome",
   *         @OA\Schema(
   *             type="string"
   *          )
   *       ),

   * )
   */
  public function listGenres(Request $request)
  {
    $data = $request->all();
    $perPage = isset($data['perPage']) ? $data['perPage'] : 10;
    $genres = Genre::when(isset($data['name']), function ($query) use ($data) {
      $query->where('name', 'like', '%' . $data['name'] . '%');
    })->paginate($perPage);
    return response($genres, 200);
  }


  /**
* @OA\Get(
*     tags={"General"},
*     path="/api/locations",
*     summary="Retrieve locations",
*     @OA\Response(
*         response="200",
*         description="The data"
*     ),
*     @OA\Parameter(
*         in="query",
*         name="name",
*         required=false,
*         example="nome",
*         @OA\Schema(
*             type="string"
*          )
*       ),

* )
*/
  public function listLocations(Request $request)
  {
    $data = $request->all();

    $perPage = isset($data['perPage']) ? $data['perPage'] : 10;

    $locations = Location::with(['city', 'city.state', 'city.state.country'])->withCount('events')->when(isset($data['name']), function ($query) use ($data) {
      $query->where('name', 'like', '%' . $data['name'] . '%');
    })->paginate($perPage);
    return response($locations, 200);
  }


  /**
 * @OA\Get(
 *     tags={"General"},
 *     path="/api/companies",
 *     summary="Retrieve companies",
 *     @OA\Response(
 *         response="200",
 *         description="The data"
 *     ),
 *     @OA\Parameter(
 *         in="query",
 *         name="name",
 *         required=false,
 *         example="nome",
 *         @OA\Schema(
 *             type="string"
 *          )
 *       ),

 * )
 */
  public function listCompanies(Request $request)
  {
    $data = $request->all();

    $sortedBy = isset($data['sortedBy']) ? $data['sortedBy'] : 'id';
    $sorted = isset($data['sorted']) ? $data['sorted'] : 'ASC';
    $perPage = isset($data['perPage']) ? $data['perPage'] : 10;

    $companies = Company::when(isset($data['name']), function ($query) use ($data) {
      $query->where('name', 'like', '%' . $data['name'] . '%');
    })
    ->with('artists')
    ->orderBy($sortedBy, $sorted)
    ->paginate($perPage);
    return response($companies, 200);
  }


  /**
   * @OA\Get(
   *     tags={"General"},
   *     path="/api/categories",
   *     summary="Retrieve categories",
   *     @OA\Response(
   *         response="200",
   *         description="The data"
   *     ),
   *     @OA\Parameter(
   *         in="query",
   *         name="name",
   *         required=false,
   *         example="nome",
   *         @OA\Schema(
   *             type="string"
   *          )
   *       ),

   * )
   */
  public function listCategories(Request $request)
  {
    $data = $request->all();
    $categories = Category::when(isset($data['name']), function ($query) use ($data) {
      $query->where('name', 'like', '%' . $data['name'] . '%');
    })->get();
    return response($categories, 200);
  }


  /**
* @OA\Get(
*     tags={"General"},
*     path="/api/eventTypes",
*     summary="Retrieve event types",
*     @OA\Response(
*         response="200",
*         description="The data"
*     ),
*     @OA\Parameter(
*         in="query",
*         name="name",
*         required=false,
*         example="nome",
*         @OA\Schema(
*             type="string"
*          )
*       ),

* )
*/
  public function listEventTypes(Request $request)
  {
    $data = $request->all();

    $sortedBy = isset($data['sortedBy']) ? $data['sortedBy'] : 'id';
    $sorted = isset($data['sorted']) ? $data['sorted'] : 'ASC';

    $eventTypes = EventType::when(isset($data['name']), function ($query) use ($data) {
      $query->where('name', 'like', '%' . $data['name'] . '%');
    })
    ->orderBy($sortedBy, $sorted)
    ->get();
    return response($eventTypes, 200);
  }

  /**
* @OA\Get(
*     tags={"General"},
*     path="/api/tags",
*     summary="Retrieve tags",
*     @OA\Response(
*         response="200",
*         description="The data"
*     ),
*     @OA\Parameter(
*         in="query",
*         name="name",
*         required=false,
*         example="nome",
*         @OA\Schema(
*             type="string"
*          )
*       ),

* )
*/
  public function listTags(Request $request)
  {
    $data = $request->all();
    $tags = Tag::when(isset($data['name']), function ($query) use ($data) {
      $query->where('name', 'like', '%' . $data['name'] . '%');
    })->get();
    return response($tags, 200);
  }


  /**
* @OA\Get(
*     tags={"General"},
*     path="/api/highlights",
*     summary="Retrieve highlights",
*     @OA\Response(
*         response="200",
*         description="The data"
*     ),
*     @OA\Parameter(
*         in="query",
*         name="name",
*         required=false,
*         example="nome",
*         @OA\Schema(
*             type="string"
*          )
*       ),
*     @OA\Parameter(
*         in="query",
*         name="type_id",
*         required=false,
*         example="nome",
*         @OA\Schema(
*             type="int"
*          )
*       ),

* )
*/
  public function listHighlights(Request $request)
  {
    $data = $request->all();
    $highlights = Highlight::with(['type'])
      ->when(isset($data['name']), function ($query) use ($data) {
        $query->where('name', 'like', '%' . $data['name'] . '%');
      })
      ->when(isset($data['type_id']), function ($query) use ($data) {
        $query->where('type_id', $data['type_id']);
      })
      ->when(isset($data['status']), function ($query) use ($data) {
        $query->where('status', $data['status']);
      })
      ->orderBy('priority', 'ASC')
      ->paginate(10);
    return response($highlights, 200);
  }

  /**
   * @OA\Get(
   *     tags={"General"},
   *     path="/api/highlightsTypes",
   *     summary="Retrieve highlights types",
   *     @OA\Response(
   *         response="200",
   *         description="The data"
   *     ),
   *     @OA\Parameter(
   *         in="query",
   *         name="name",
   *         required=false,
   *         example="nome",
   *         @OA\Schema(
   *             type="string"
   *          )
   *       ),
   * )
   */
  public function listHighlightsTypes(Request $request)
  {
    $data = $request->all();
    $highlightsTypes = HighlightType::when(isset($data['name']), function ($query) use ($data) {
      $query->where('name', 'like', '%' . $data['name'] . '%');
    })
      ->get();
    return response($highlightsTypes, 200);
  }


  /**
   * @OA\Get(
   *     tags={"General"},
   *     path="/api/ads",
   *     summary="Retrieve ads",
   *     @OA\Response(
   *         response="200",
   *         description="The data"
   *     ),
   *     @OA\Parameter(
   *         in="query",
   *         name="name",
   *         required=false,
   *         example="nome",
   *         @OA\Schema(
   *             type="string"
   *          )
   *       ),
   *     @OA\Parameter(
   *         in="query",
   *         name="type_id",
   *         required=false,
   *         example="nome",
   *         @OA\Schema(
   *             type="int"
   *          )
   *       ),
   *     @OA\Parameter(
   *         in="query",
   *         name="category_id",
   *         required=false,
   *         example="1,3,2",
   *         @OA\Schema(
   *             type="array"
   *          )
   *       ),
   * )
   */
  public function listAds(Request $request)
  {
    $data = $request->all();
    $ads = Ad::with(['categories'])
      ->when(isset($data['name']), function ($query) use ($data) {
        $query->where('name', 'like', '%' . $data['name'] . '%');
      })
      ->when(isset($data['category_id']), function ($query) use ($data) {
        $query->whereHas('category', function ($query) use ($data) {
          $query->whereIn('id', explode(",", $data['category_id']));
        });
      })
      ->when(!isset($data['filterAdmin']), function ($query) use ($data) {
        $query->where('status', 1);
      })
      ->when(isset($data['filterAdmin']), function ($query) use ($data) {
        $query->orderBy('updated_at', 'DESC');
      })
      ->paginate(10);
    return response($ads, 200);
  }


  /**
   * @OA\Get(
   *     tags={"General"},
   *     path="/api/users",
   *     summary="Retrieve users",
   *     @OA\Response(
   *         response="200",
   *         description="The data"
   *     ),
   *     @OA\Parameter(
   *         in="query",
   *         name="name",
   *         required=false,
   *         example="nome",
   *         @OA\Schema(
   *             type="string"
   *          )
   *       ),
   * )
   */
  public function listUsers(Request $request)
  {
    $data = $request->all();
    $users = User::with(['role'])->when(isset($data['name']), function ($query) use ($data) {
      $query->where('name', 'like', '%' . $data['name'] . '%');
    })
      ->paginate(10);
    return response($users, 200);
  }


  /**
   * @OA\Get(
   *     tags={"General"},
   *     path="/api/authors",
   *     summary="Retrieve authors",
   *     @OA\Response(
   *         response="200",
   *         description="The data"
   *     ),
   *     @OA\Parameter(
   *         in="query",
   *         name="position",
   *         required=false,
   *         example="fundador",
   *         @OA\Schema(
   *             type="string"
   *          )
   *       ),
   * )
   */
  public function listAuthors(Request $request)
  {
    $data = $request->all();
    $authors = Author::with(['social_media'])
        ->withCount('news')
        ->when(isset($data['position']), function ($query) use ($data) {
      $query->where('position', $data['position']);
    })
      ->paginate(10);
    return response($authors, 200);
  }


  /**
   * @OA\Get(
   *     tags={"General"},
   *     path="/api/adsCategories",
   *     summary="Retrieve ads categories",
   *     @OA\Response(
   *         response="200",
   *         description="The data"
   *     ),
   *     @OA\Parameter(
   *         in="query",
   *         name="name",
   *         required=false,
   *         example="nome",
   *         @OA\Schema(
   *             type="string"
   *          )
   *       ),
   * )
   */
  public function listAdsCategories(Request $request)
  {
    $data = $request->all();
    $adsCategories = AdCategory::when(isset($data['name']), function ($query) use ($data) {
      $query->where('name', 'like', '%' . $data['name'] . '%');
    })
      ->get();
    return response($adsCategories, 200);
  }

  /**
   * @OA\Get(
   *     tags={"General"},
   *     path="/api/togethers",
   *     summary="Retrieve togethers",
   *     @OA\Response(
   *         response="200",
   *         description="The data"
   *     ),
   *     @OA\Parameter(
   *         in="query",
   *         name="name",
   *         required=false,
   *         example="nome",
   *         @OA\Schema(
   *             type="string"
   *          )
   *       ),
   * )
   */
  public function listTogethers(Request $request)
  {
    $data = $request->all();
    $perPage = isset($data['perPage']) ? $data['perPage'] : 10;

    $togethers = Together::with('category', 'events')
      ->when(isset($data['status']), function ($query) use ($data) {
        $query->where('status', $data['status']);
    })
      ->when(isset($data['name']), function ($query) use ($data) {
        $query->where('name', 'like', '%' . $data['name'] . '%');
     })
     ->when(isset($data['category']), function ($query) use ($data) {
      $query->where('category_id', $data['category']);
   })
      ->when(isset($data['event_id']), function ($query) use ($data) {
        $query->whereHas('events', function ($query) use ($data) {
          $query->whereIn('id', explode(",", $data['event_id']));
        });
      })
      ->orderBy(function ($query) {
        $query->select('priority')
            ->from('categories_together')
            ->whereColumn('togethers.category_id', 'categories_together.id')
            ->limit(1);
      }, 'asc')
      ->orderBy('togethers.priority', 'ASC')
      ->paginate($perPage);

    return response($togethers, 200);
          /*
      ALTER TABLE togethers
    ADD CONSTRAINT together_category_FK_1
    FOREIGN KEY (category_id)
    REFERENCES categories_together(id);
      */
  }




  /**
   * @OA\Get(
   *     tags={"General"},
   *     path="/api/adsCategories",
   *     summary="Retrieve ads categories",
   *     @OA\Response(
   *         response="200",
   *         description="The data"
   *     ),
   *     @OA\Parameter(
   *         in="query",
   *         name="name",
   *         required=false,
   *         example="nome",
   *         @OA\Schema(
   *             type="string"
   *          )
   *       ),
   * )
   */
  public function listTogethersCategories(Request $request)
  {
    $data = $request->all();
    $togethersCategories = TogetherCategory::when(isset($data['name']), function ($query) use ($data) {
      $query->where('name', 'like', '%' . $data['name'] . '%');
    })
    ->orderBy('priority', 'asc')
      ->get();
    return response($togethersCategories, 200);
  }

  /**
   * @OA\Get(
   *     tags={"General"},
   *     path="/api/socialMedia",
   *     summary="Retrieve social media",
   *     @OA\Response(
   *         response="200",
   *         description="The data"
   *     ),
   *     @OA\Parameter(
   *         in="query",
   *         name="name",
   *         required=false,
   *         example="nome",
   *         @OA\Schema(
   *             type="string"
   *          )
   *       ),
   * )
   */
  public function listSocialMedia(Request $request)
  {
    $data = $request->all();
    $socialMedia = SocialMedia::when(isset($data['name']), function ($query) use ($data) {
      $query->where('name', 'like', '%' . $data['name'] . '%');
    })
      ->get();
    return response($socialMedia, 200);
  }


  public function linkTree()
  {
    $linktree = LinkTree::get();
    return response($linktree, 200);
  }

  public function roleUser()
  {
    $rolesAuthor = RoleUser::get();
    return response($rolesAuthor, 200);
  }

  public function getRedirect(Request $request)
  {
    $data = $request->all();
    $redirect = MassiveRedirect::where('from_url', $data['from_url'])->first();
    return response($redirect, 200);
  }

  public function listRedirects(Request $request)
  {
    $data = $request->all();
    $redirect = MassiveRedirect::
    when(isset($data['from_url']), function ($query) use ($data) {
      $query->where('from_url', 'like', '%' . $data['from_url'] . '%');
    })
    ->when(isset($data['to_url']), function ($query) use ($data) {
      $query->where('to_url', 'like', '%' . $data['to_url'] . '%');
    })
    ->paginate(10);
    return response($redirect, 200);
  }

  public function listSiteData(Request $request)
  {
    $data = $request->all();
    $siteData = SiteData::where('name', $request['name'])->get();

    $siteDataArray = $siteData->toArray();

    foreach ($siteDataArray as &$item) {
      if (isset($item['value'])) {
          $item['value'] = json_decode(json_encode($item['value']), true);

          if (is_array($item['value'])) {
              usort($item['value'], function ($a, $b) {
                  return $a['priority'] - $b['priority'];
              });
          }
      }
  }


    return response()->json($siteDataArray, 200);
  }

}
