<?php

namespace App\Http\Controllers;

use App\Models\SiteData;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;

class SiteDataController extends Controller
{
    public function index()
    {
        return Inertia::render('SiteData/List', [
            'siteData' => SiteData::all()
        ]);
    }

    public function edit($id)
    {

        $sitedata = SiteData::findOrFail($id);

        if($sitedata->name == 'analytics') {
            return Inertia::render('SiteData/Analytics/Edit', [
                'siteData' => $sitedata,
            ]); 
        }
        return Inertia::render('SiteData/Sponsors/Edit', [
            'siteData' => $sitedata,
        ]);       
    }

    public function create(Request $request)
    {
        return Inertia::render('SiteData/Create');
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        $siteData = SiteData::findOrFail($id);
        $siteData->update([
            'name' => $data['name'],
            'value' => $data['value'],
        ]);
        $siteData->update($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $siteData = SiteData::create($data);

        return response()->json($siteData);
    }

    public function destroy(Request $request)
    {
        $siteData = SiteData::find($request['siteData']);
        $siteData->delete();
    }
}
