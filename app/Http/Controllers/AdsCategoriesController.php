<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\Genre;
use App\Models\AdCategory;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;

class AdsCategoriesController extends Controller
{
    public function index()
    {
        return Inertia::render('AdCategory/List');
    }

    public function edit(Request $request)
    {
        $ad = AdCategory::findOrFail($request['adCategory']);
        return Inertia::render('AdCategory/Edit', [
            'adCategory' => $ad,
        ]);
    }

    public function create(Request $request)
    {
        return Inertia::render('AdCategory/Create');
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $category = AdCategory::find($request['id']);
        $category->update($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $adCategory = AdCategory::create($data);
    }




}
