<?php

namespace App\Http\Controllers;

use App\Models\TogetherCategory;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;

class TogethersCategoriesController extends Controller
{

    public function index()
    {
        return Inertia::render('TogetherCategory/List');
    }

    public function create()
    {
        return Inertia::render('TogetherCategory/Create');
    }

    public function edit(Request $request)
    {
        $togetherCategory = TogetherCategory::findOrFail($request['togethersCategory']);
        return Inertia::render('TogetherCategory/Edit', [
            'togetherCategory' => $togetherCategory,
        ]);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $togetherCategory = TogetherCategory::find($request['id']);
        $togetherCategory->update($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $togetherCategory = TogetherCategory::create($data);
    }

}
