<?php
namespace App\Http\Controllers;

use App\Models\HighlightType;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;

class HighlightsTypesController extends Controller
{
    
    public function index()
    {
        return Inertia::render('HighlightType/List');
    }

    public function edit(Request $request)
    {
        $highlightType = HighlightType::findOrFail($request['highlightsType']);
        return Inertia::render('HighlightType/Edit', [
            'highlight' => $highlightType,
        ]);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $highlight = HighlightType::find($request['highlightsType']);
        $highlight->update($data);
    }

    public function create(Request $request)
    {
        return Inertia::render('HighlightType/Create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $highlight = HighlightType::create($data);
    }
    



}
