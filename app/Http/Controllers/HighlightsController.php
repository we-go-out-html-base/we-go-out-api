<?php
namespace App\Http\Controllers;

use App\Models\Highlight;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;

class HighlightsController extends Controller
{
    public function index()
    {
        return Inertia::render('Highlight/List');
    }

    public function edit(Request $request)
    {
        $highlight = Highlight::with(['type'])->findOrFail($request['highlight']);
        return Inertia::render('Highlight/Edit', [
            'highlight' => $highlight,
        ]);
    }

    public function create(Request $request)
    {
        return Inertia::render('Highlight/Create');
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $highlight = Highlight::find($request['id']);
        $highlight->update($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $highlight = Highlight::create($data);
    }
}
