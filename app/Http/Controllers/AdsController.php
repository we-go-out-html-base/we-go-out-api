<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\Genre;
use App\Models\AdCategory;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;

class AdsController extends Controller
{
    public function index()
    {
        return Inertia::render('Ad/List');
    }

    public function edit(Request $request)
    {
        $ad = Ad::with(['categories'])->findOrFail($request['ads2']);
        return Inertia::render('Ad/Edit', [
            'ad' => $ad,
        ]);
    }

    public function create(Request $request)
    {
        return Inertia::render('Ad/Create');
    }

    public function update(Request $request)
    {
        $data = $request->all();
        // dd($data);
        $event = Ad::find($request['id']);
        $event->update($data);
        $event->categories()->sync($data['categories']);
        // return Redirect::route('ads.list');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $ad = Ad::create($data);
        $ad->categories()->sync($data['categories']);

        return response()->json($ad);
    }

    public function destroy(Request $request)
    {
        $ad = Ad::find($request['ads2']);
        $ad->delete();
    }


    public function uploadImage(Request $request)
    {
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $image = $request->file('image');
            $imageName = $image->getClientOriginalName();
            $image->move(public_path('images/ads'), $imageName);
            return response()->json($imageName);
        }

        return response()->json('Erro ao fazer upload da imagem.', 500);
    }

    public function listCategories()
    {
        return Inertia::render('AdCategory/List');
    }

    public function createCategory()
    {
        return Inertia::render('AdCategory/Create');
    }

    public function editCategory(Request $request)
    {
        $ad = AdCategory::findOrFail($request['id']);
        return Inertia::render('AdCategory/Edit', [
            'adCategory' => $ad,
        ]);
    }

    public function updateCategory(Request $request)
    {
        $data = $request->all();
        $category = AdCategory::find($request['id']);
        $category->update($data);
        return Redirect::route('ads.categories.list');
    }

    public function storeCategory(Request $request)
    {
        $data = $request->all();
        $adCategory = AdCategory::create($data);
        return Redirect::route('ads.list');
    }

}
