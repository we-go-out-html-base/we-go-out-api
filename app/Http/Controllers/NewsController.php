<?php

namespace App\Http\Controllers;


use App\Models\Author;
use App\Models\Artist;
use App\Models\Event;
use App\Models\Company;
use App\Models\Genre;
use App\Models\News;
use App\Models\Category;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use App\Helpers\AddVideoEmbedTags;
use App\Helpers\RemoveDiacritic;
use App\Helpers\HandleSlugString;
use App\Helpers\RemoveStyleForAmp;
use App\Http\Requests\UpdateNewsRequest;
use App\Http\Requests\StoreNewsRequest;

class NewsController extends Controller
{
    public function index()
    {
        return Inertia::render('News/List');
    }

    public function edit(Request $request)
    {
        // dd($request['release']);
        $news = News::with(['artists', 'author', 'events'])->findOrFail($request['news']);
        return Inertia::render('News/Edit', [
            'news' => $news,
            'categories' => Category::all(),
            'authors' => Author::all(),
            'artists' => Artist::all(),
            'events' => Event::where('start_date', '>=',  Carbon::now())->get()
        ]);
    }

    public function create(Request $request)
    {
        return Inertia::render('News/Create', [
            'categories' => Category::all(),
            'authors' => Author::all(),
            'artists' => Artist::all(),
            'events' => Event::where('start_date', '>=',  Carbon::now())->get()
        ]);
    }

    public function update(UpdateNewsRequest $request)
    {
        $data = $request->all();
        
            $verificaslug = News::where('slug', $data['slug'])->where('id', '<>', $data['id'])->count();
        
            if ($verificaslug > 0) {
                return response()->json(['error' => 'O slug já está sendo usado por outro conteúdo'], 422);
            }

            $data['content'] = RemoveStyleForAmp::remove($data['content']);

            $data['slug'] = HandleSlugString::handle($data['slug']);
            $data['slug'] = RemoveDiacritic::remove($data['slug']);
            array_key_exists('content', $data) && $data['content'] = AddVideoEmbedTags::addTags($data['content']); 
            
            $new = News::find($request['id']);
            $new->update($data);
            $new->artists()->sync($data['artists']);
            $new->events()->sync($data['events']);
        
            return response()->json(['message' => 'Evento atualizado com sucesso']);
    }

    public function store(StoreNewsRequest $request)
    {
        $data = $request->all();

        $validated = $request->validate([
            'slug' => 'required|unique:news',
        ],[
            'slug.required' => 'O campo slug é obrigatório.',
            'slug.unique' => "O slug deve ser único"
        ]);

        $data['content'] = RemoveStyleForAmp::remove($data['content']);
        
        $data['slug'] = HandleSlugString::handle($data['slug']);
        $data['slug'] = RemoveDiacritic::remove($data['slug']);
        array_key_exists('content', $data) && $data['content'] = AddVideoEmbedTags::addTags($data['content']);

        if($validated){
            $new = News::create($data);
            $new->artists()->sync($data['artists']);
            $new->events()->sync($data['events']);

            return response()->json($new);
        }
    }

    public function destroy(Request $request)
    {
        $new = News::find($request['news']);
        $new->delete();
    }
}