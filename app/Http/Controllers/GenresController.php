<?php

namespace App\Http\Controllers;


use App\Models\Genre;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;

class GenresController extends Controller
{
    public function index()
    {
        return Inertia::render('Genre/List');
    }

    public function edit(Request $request)
    {
        $genre = Genre::findOrFail($request['genre']);
        return Inertia::render('Genre/Edit', ['genre' => $genre]);
    }

    public function create(Request $request)
    {
        return Inertia::render('Genre/Create');
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $event = Genre::find($request['id']);
        $event->update($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $genre = Genre::create($data);
    }


}
