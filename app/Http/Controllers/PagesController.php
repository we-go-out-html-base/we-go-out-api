<?php

namespace App\Http\Controllers;


use App\Models\Page;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;

class PagesController extends Controller
{
    public function index()
    {
        return Inertia::render('Pages/List');
    }

    public function edit(Request $request)
    {
        // dd($request['release']);
        $page = Page::findOrFail($request['page']);
        return Inertia::render('Pages/Edit', [
            'page' => $page,
        ]);
    }

    public function create(Request $request)
    {
        return Inertia::render('Pages/Create');
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $page = Page::find($request['id']);
        $page->update($data);

    }

    public function store(Request $request)
    {
        $data = $request->all();
        $release = Page::create($data);
    }

    



}
