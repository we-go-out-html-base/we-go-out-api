<?php
namespace App\Http\Controllers;

use App\Models\RoleAuthor;
use Illuminate\Http\Request;
use Inertia\Inertia;

class RoleAuthorController extends Controller
{
    public function index()
    {
        return Inertia::render('Author/Roles', [
            'rolesAuthors' => RoleAuthor::all()
        ]);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $roleAuthor = RoleAuthor::find($request['roleAuthor']);
        $roleAuthor->update($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $roleAuthor = RoleAuthor::create($data);
    }

    public function destroy(Request $request)
    {
        $roleAuthor = RoleAuthor::find($request['roleAuthor']);
        $roleAuthor->delete();
    }
}