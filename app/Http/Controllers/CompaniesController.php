<?php

namespace App\Http\Controllers;


use App\Models\Company;
use App\Models\Artist;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;

class CompaniesController extends Controller
{
    public function index()
    {
        return Inertia::render('Companies/List');
    }

    public function edit(Request $request)
    {
        // dd($request['release']);
        $company = Company::with('artists')->findOrFail($request['company']);
        return Inertia::render('Companies/Edit', [
            'company' => $company,
            'artists' => Artist::all()
        ]);
    }

    public function create(Request $request)
    {
        return Inertia::render('Companies/Create', [
            'artists' => Artist::all()
        ]);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $company = Company::find($request['id']);
        $company->update($data);
        $company->artists()->sync($data['artists']);

    }

    public function store(Request $request)
    {
        $data = $request->all();
        $company = Company::create($data);
        $company->artists()->sync($data['artists']);
    }

    public function destroy(Request $request)
    {
        $company = Company::find($request['company']);
        $company->delete();
    }

    



}
