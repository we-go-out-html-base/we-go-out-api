<?php

namespace App\Http\Requests;

class UpdateAuthorRequest extends BaseRequest
{
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'image' => 'required',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'O nome é obrigatório',
            'image.required' => 'A imagem é obrigatória'
        ];
    }
}
