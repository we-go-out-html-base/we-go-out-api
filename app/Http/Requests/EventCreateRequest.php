<?php

namespace App\Http\Requests;

class EventCreateRequest extends BaseRequest
{
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'slug' => 'required',
            'event_type_id' => 'required',
//            'location_id' => 'required',
//            'has_discount' => 'required',
            'about' => 'required',
//            'organizer_description' => 'required',
            'organizer_name' => 'required',
            'organizer_email' => 'required',
            'organizer_phone' => 'required',
//            'organizer_url' => 'required',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'O nome é obrigatório',
            'slug.required' => 'A URL do evento é obrigatória',
            'event_type_id.required' => 'A categoria é obrigatória',
            'location_id.required' => 'O local é obrigatório',
//            'has_discount.required' => 'A opção de desconto é obrigatória',
            'about.required' => 'O sobre o evento é obrigatório',
            'organizer_description.required' => 'O campo links úteis é obrigatório',
            'organizer_name.required' => 'O nome da agencia produtora é obrigatório',
            'organizer_email.required' => 'O email para contato é obrigatório',
            'organizer_phone.required' => 'O telefone de contato é obrigatório',
            'organizer_url.required' => 'A URL da produtora é obrigatória',
        ];
    }
}
