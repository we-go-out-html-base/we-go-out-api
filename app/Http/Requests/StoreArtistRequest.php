<?php

namespace App\Http\Requests;

class StoreArtistRequest extends BaseRequest
{    
    public function rules()
    {
        $rules = [
            'slug' => 'required|unique:artists',
            'name' => 'required',
            // 'embed' => 'required',
            'image' => 'required',
            'meta_description' => 'required',
            'key_words' => 'required'
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'O nome é obrigatório',
            'slug.required' => 'O slug é obrigatório',
            'slug.unique' => 'O slug deve ser único',
            // 'embed' => 'O embed é obrigatório',
            'image' => 'Imagem é obrigatória',
            'meta_description' => 'Meta description é obrigatório',
            'key_words' => 'Palavras-chave são obrigatórias'
        ];
    }
}
