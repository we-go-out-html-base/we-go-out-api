<?php

namespace App\Http\Requests;

class StoreReleaseRequest extends BaseRequest
{    
    public function rules()
    {
        $rules = [
            'slug' => 'required|unique:releases',
            'name' => 'required',
            'date' => 'required',
            'embed' => 'required',
            'image' => 'required',
            'meta_description' => 'required',
            'key_words' => 'required',
            'artists' => 'required'
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'O nome é obrigatório',
            'slug.required' => 'O slug é obrigatório',
            'slug.unique' => 'O slug deve ser único',
            'embed' => 'O embed é obrigatório',
            'date' => 'A data de lançamento é obrigatória',
            'image' => 'Imagem é obrigatória',
            'meta_description' => 'Meta description é obrigatório',
            'key_words' => 'Palavras-chave são obrigatórias',
            'artists' => 'Artistas é uma informação obrigatória'
        ];
    }


}
