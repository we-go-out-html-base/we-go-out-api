<?php

namespace App\Http\Requests;

class UpdateReleaseRequest extends BaseRequest
{    
    public function rules()
    {
        $rules = [
            'slug' => 'required',
            'name' => 'required',
            'date' => 'required',
            'embed' => 'required',
            'image' => 'required',
            'meta_description' => 'required',
            'key_words' => 'required',
            'artists' => 'required'
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'O nome é obrigatório',
            'slug.required' => 'O slug é obrigatório',
            'embed' => 'O embed é obrigatório',
            'date' => 'A data de lançamento é obrigatória',
            'image' => 'Imagem é obrigatória',
            'meta_description' => 'Meta description é obrigatório',
            'key_words' => 'Palavras-chave são obrigatórias',
            'artists' => 'Artistas é uma informação obrigatória'
        ];
    }
}
