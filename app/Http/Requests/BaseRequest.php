<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

abstract class BaseRequest extends FormRequest
{
    protected function failedValidation($validator)
    {
        $result = '';
        foreach( $validator->messages()->toArray() as $message ) {
            $result .= implode(' ', $message ).'<br /> ';
        }

        throw ValidationException::withMessages([
            'unique_message' => $result
        ]);
    }
}