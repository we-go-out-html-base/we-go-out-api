<?php

namespace App\Http\Requests;

class StoreEventRequest extends BaseRequest
{
    public function rules()
    {
        $rules = [
            'slug' => 'required|unique:events',
            'name' => 'required',
            'event_type_id' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'image' => 'required',
            'meta_description' => 'required',
            'keywords' => 'required',
            'publish_date' => 'required'
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'O nome é obrigatório',
            'slug.required' => 'O slug é obrigatório',
            'slug.unique' => 'O slug deve ser único',
            'event_type_id' => 'A categoria é obrigatória',
            'start_date' => 'Data de início é obrigatório',
            'end_date' => 'Data de fim é obrigatório',
            'image' => 'Imagem é obrigatória',
            'meta_description' => 'Meta description é obrigatório',
            'keywords' => 'Palavras-chave são obrigatórias',
            'publish_date' => 'A data de publicação é obrigatória'
        ];
    }
}
