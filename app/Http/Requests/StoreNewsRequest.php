<?php

namespace App\Http\Requests;

class StoreNewsRequest extends BaseRequest
{    
    public function authorize()
    {
        return true; // Defina isso conforme sua necessidade de autorização
    }

    public function rules()
    {
        $rules = [
            'slug' => 'required|unique:news',
            'title' => 'required',
            // 'embed' => 'required',
            'image' => 'required',
            'meta_description' => 'required',
            'key_words' => 'required',
            'public_date' => 'required',
            'category_id' => 'required',
            'content' => 'required',
            'author_id' => 'required'
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'title.required' => 'O nome é obrigatório',
            'slug.required' => 'O slug é obrigatório',
            'slug.unique' => 'O slug deve ser único',
            'embed' => 'O embed é obrigatório',
            'image' => 'Imagem é obrigatória',
            'meta_description' => 'Meta description é obrigatório',
            'key_words' => 'Palavras-chave são obrigatórias',
            'public_date' => 'A data de publicação é obrigatória',
            'category_id' => 'A categoria é obrigatória',
            'content' => 'O conteúdo é obrigatório',
            'author_id' => 'O autor é obrigatório'
        ];
    }
}
