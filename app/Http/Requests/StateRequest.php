<?php

namespace App\Http\Requests;

class StateRequest extends BaseRequest
{    
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'abbr' => 'required',
            'country_id' => 'required',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'O nome é obrigatório',
            'abbr.required' => 'O código é obrigatório',
            'country_id.required' => 'O país é obrigatório'
        ];
    }
}
