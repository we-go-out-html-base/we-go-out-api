<?php

namespace App\Http\Requests;

class CityRequest extends BaseRequest
{
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'state_id' => 'required',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'O nome é obrigatório',
            'state_id.required' => 'O estado é obrigatório'
        ];
    }
}
