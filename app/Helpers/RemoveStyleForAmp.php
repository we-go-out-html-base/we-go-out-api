<?php
namespace App\Helpers;

class RemoveStyleForAmp {

    public static function remove($string){
        $callback = function ($matches) {
            
            $allowed_rule = 'height: 160px;';
            $styles = explode(';', $matches[2]);
            $filtered_styles = array_filter($styles, function ($style) use ($allowed_rule) {
                return stripos(trim($style), 'height: 160px') !== false;
            });
            $filtered_style = implode('; ', $filtered_styles);
        
            return $matches[1] . $filtered_style . $matches[3];
        };
        $pattern = '/(<[^>]+style=")([^"]*)("[^>]*>)/i';
        return preg_replace_callback($pattern, $callback, $string);
    }

}