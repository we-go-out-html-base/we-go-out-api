<?php
namespace App\Helpers;

class HandleSlugString {

    public static function handle($string) {
        $slug = str_replace(' ', '-', $string);
        $slug = strtolower($slug);
        $slug = preg_replace('/-+/', '-', $slug);
        return $slug;
    }
}