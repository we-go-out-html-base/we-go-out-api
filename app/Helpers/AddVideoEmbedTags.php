<?php
namespace App\Helpers;

class AddVideoEmbedTags {

    public static function addTags($string){

        $pattern = '/<iframe\s+[^>]*>.*?<\/iframe>/is';
        // $pattern = '/<iframe\s+[^>]*src=["\'](?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/embed\/|youtu\.be\/)([\w-]+)["\'][^>]*>.*?<\/iframe>/is';

        $callback = function ($matches) {
            if (strpos($matches[0], 'allow=') === false) {
                $matches[0] = str_replace('<iframe', '<iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"', $matches[0]);
            }
            if (strpos($matches[0], 'referrerpolicy=') === false) {
                $matches[0] = str_replace('<iframe', '<iframe referrerpolicy="strict-origin-when-cross-origin"', $matches[0]);
            }
            return $matches[0];
        };
        
        $string = preg_replace_callback($pattern, $callback, $string);

        return $string;
    }

}
