<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class SocialMedia extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'social_media';

    protected $fillable = ['social_media_id', 'artist_id', 'link'];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function artists(): BelongsToMany
    {
        return $this->BelongsTo(Artist::class, 'artists_social_media', 'social_media_id', 'artist_id');
    }
}
