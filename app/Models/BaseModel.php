<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class BaseModel extends Model {

    use HasFactory;
    use SoftDeletes;

    protected $appends = [
        'images'
    ];


    public function getImagesAttribute()
    {
        // return $this->getTable
        // $imagesDir = dirname($this->image);
        $imagesDir = public_path() . '/images/' . $this->getTable() . '/' . $this->id;
        $filesArray = [];
        if(is_dir($imagesDir)){
            $files = scandir($imagesDir); 
            foreach($files as $file){
                if(str_contains($file, 'thumb_' . basename($this->image))){
                    $filesArray['thumb'] = dirname($this->image) . '/thumb_' . basename($this->image);
                }else if(str_contains($file, 'medium_' . basename($this->image))){
                    $filesArray['medium'] = dirname($this->image) . '/medium_' . basename($this->image);
                }else if(str_contains($file, 'large_' . basename($this->image))){
                    $filesArray['large'] = dirname($this->image) . '/large_' . basename($this->image);
                }else if(str_contains($file, 'full_' . basename($this->image))){
                    $filesArray['full'] = dirname($this->image) . '/full_' . basename($this->image);
                }
            }
            return $filesArray;
        }
        return $filesArray;
    }

    
}