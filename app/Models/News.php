<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
// use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{

    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'news';

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'pivot'
    ];

    protected $fillable = [
        "title",
        "content",
        "category_id",
        "image",
        "slug",
        "author_id",
        "key_words",
        "meta_description",
        "is_main",
        "is_draft",
        "public_date",
        "status",
        "search_terms",
        "selected_events"
    ];

    protected $casts = [
        'public_date' => 'datetime:Y-m-d H:i'
    ];

    protected $appends = ['formatted_content', 'images'];

    // protected function content(): Attribute
    // {
    //     return Attribute::make(
    //         get: fn (string $value) => preg_replace("/<p[^>]*><br><\\/p[^>]*>/", '', $value),
    //     );
    // }

    public function artists(): BelongsToMany
    {
        return $this->BelongsToMany(Artist::class, 'artists_news', 'new_id', 'artist_id');
    }

    public function author(): BelongsTo
    {
        return $this->BelongsTo(Author::class, 'author_id');
    }

    public function category(): BelongsTo
    {
        return $this->BelongsTo(Category::class, 'category_id');
    }

    public function events(): BelongsToMany
    {
        return $this->BelongsToMany(Event::class, 'events_news', 'new_id', 'event_id');
    }

    public function getFormattedContentAttribute()
    {
        $pattern = '/\[\[([a-zA-Z]+):(\d+)\]\]/';
        $description = preg_replace_callback($pattern, function ($matches) {
            $modelClass = 'App\Models\\' . ucfirst($matches[1]);
            $modelId = $matches[2];

            $model = $modelClass::find($modelId);

            if ($model) {
                if ($matches[1] == 'event') {
                    // se é evento
                    return " -- " . $model->name . '-' . $model->start_date . ' -- ';
                }
                if ($matches[1] == 'release') {
                    // se é lancamento
                    return " -- " . $model->name . '-' . $model->description . ' -- ';
                }
                if ($matches[1] == 'artist') {
                    // se é artista
                    return " -- " . $model->name . ' -- ';
                }
                if ($matches[1] == 'news') {
                    // se é noticia
                    return " -- " . $model->title . ' -- ';
                }
            }

            return $matches[0]; // Mantém o shortcode original caso o modelo não seja encontrado
        }, $this->content);

        return $description;
    }

    public function getImagesAttribute()
    {
        $imagesDir = public_path() . '/images/' . $this->getTable() . '/' . $this->id;
        $filesArray = [];
        if(is_dir($imagesDir)){
            $files = scandir($imagesDir);
            foreach($files as $file){
                if(str_contains($file, 'thumb_' . basename($this->image))){
                    $filesArray['thumb'] = dirname($this->image) . '/thumb_' . basename($this->image);
                }else if(str_contains($file, 'medium_' . basename($this->image))){
                    $filesArray['medium'] = dirname($this->image) . '/medium_' . basename($this->image);
                }else if(str_contains($file, 'large_' . basename($this->image))){
                    $filesArray['large'] = dirname($this->image) . '/large_' . basename($this->image);
                }else if(str_contains($file, 'full_' . basename($this->image))){
                    $filesArray['full'] = dirname($this->image) . '/full_' . basename($this->image);
                }
            }
            return $filesArray;
        }
        return $filesArray;
    }
}
