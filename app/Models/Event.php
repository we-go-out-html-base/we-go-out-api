<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Carbon\Carbon;

class Event extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'events';

    protected $fillable = [
        "name",
        "coupon_code",
        "details",
        "keywords",
        "name",
        "slug",
        "location_id",
        "review_id",
        "start_date",
        "end_date",
        "tickets_link",
        "image",
        "event_type_id",
        "price",
        "host_links",
        "playlist",
        "details",
        "about",
        "guide_link",
        "keywords",
        "meta_description",
        "publish_date",
        "organizer_name",
        "organizer",
        "organizer_url",
        "organizer_phone",
        "organizer_email",
        "organizer_description",
        "created_by",
        "is_draft",
        "is_main",
        "FAQ",
        "has_discount",
        "search_terms"
    ];

    protected $dates = [
        'start_date',
        'end_date',
        'publish_date'
    ];

    protected $casts = [
        'FAQ' => 'array',
        'host_links' => 'array',
        'start_date' => 'datetime:Y-m-d H:i',
        'end_date' => 'datetime:Y-m-d H:i',
        'publish_date' => 'datetime:Y-m-d H:i'
    ];

    protected $hidden = [
        'updated_at',
        'deleted_at'
    ];

    public function artists(): BelongsToMany
    {
        return $this->BelongsToMany(Artist::class, 'events_artists', 'event_id', 'artist_id');
    }

    public function news(): BelongsToMany
    {
        return $this->belongsToMany(News::class, 'events_news', 'event_id', 'new_id')
            ->select('id', 'title', 'image', 'category_id', 'public_date', 'slug')
            ->where('is_draft', 0)
            ->where('public_date', '<=', Carbon::now())
            ->orderBy('public_date', 'DESC')
            ->with('category');
    }

    public function eventType(): BelongsTo
    {
        return $this->BelongsTo(EventType::class, 'event_type_id');
    }

    public function location(): BelongsTo
    {
        return $this->BelongsTo(Location::class, 'location_id');
    }

    public function questions(): BelongsToMany
    {
        return $this->BelongsToMany(Question::class, 'events_questions', 'event_id', 'question_id');
    }

    public function togethers(): BelongsToMany
    {
        return $this->BelongsToMany(Together::class, 'togethers_events', 'event_id', 'together_id')->where('status', 1);
    }

    public function linkedEvents(): BelongsToMany
    {
        return $this->BelongsToMany(self::class, 'linked_events', 'event_id', 'linked_event_id')->where('is_draft', 0)->with('location');
    }

    public function review(): BelongsTo
    {
        return $this->BelongsTo(News::class, 'review_id');
    }
}
