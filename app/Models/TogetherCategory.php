<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TogetherCategory extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories_together';

    protected $fillable = ['name', 'priority'];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];



}
