<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


class Release extends BaseModel
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'releases';

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $fillable = [
        "name",
        "type_id",
        "date",
        "description",
        "link",
        "embed",
        "slug",
        "key_words",
        "meta_description",
        "is_draft",
        "is_main",
        "image",
        "organizer",
        "organizer_url",
        "organizer_phone",
        "organizer_email",
        "organizer_description",
        "created_by"
    ];



    public function artists(): BelongsToMany
    {
        return $this->BelongsToMany(Artist::class, 'releases_artists', 'release_id', 'artist_id');
    }

    public function companies(): BelongsToMany
    {
        return $this->BelongsToMany(Company::class, 'releases_companies', 'release_id', 'company_id');
    }

    public function genres(): BelongsToMany
    {
        return $this->BelongsToMany(Genre::class, 'releases_genres', 'release_id', 'genre_id');
    }

    public function type(): BelongsTo
    {
        return $this->BelongsTo(TypeRelease::class, 'type_id');
    }

}
