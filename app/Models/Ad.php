<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;


class Ad extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ads';

    protected $fillable = [
        'name',
        'url',
        'image_asset_mobile',
        'image_asset_desktop',
        'status'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function categories(): BelongsToMany
    {
        return $this->BelongsToMany(AdCategory::class, 'ads_categories', 'ad_id', 'category_ad_id');
    }

}
