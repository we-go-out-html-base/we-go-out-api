<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Artist extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'artists';

    protected $fillable = [
        "name",
        "slug",
        "image",
        "biography",
        "embed",
        "key_words",
        "meta_description",
        "is_main",
        "is_draft",
        "country_id"
    ];

    protected $hidden = [
        'pivot',
        'youtube_link',
        'instagram_link',
        'spotify_link',
        'twitter_link',
        'facebook_link',
        'created_at',
        'deleted_at'
    ];


    public function eventType(): BelongsTo
    {
        return $this->BelongsTo(EventType::class, 'event_type_id');
    }

    public function genres(): BelongsToMany
    {
        return $this->BelongsToMany(Genre::class, 'artists_genres', 'artist_id', 'genre_id');
    }

    public function news(): BelongsToMany
    {
        return $this->BelongsToMany(News::class, 'artists_news', 'artist_id', 'new_id');
    }

    public function companies(): BelongsToMany
    {
        return $this->BelongsToMany(Company::class, 'artists_companies', 'artist_id', 'company_id');
    }

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function events(): BelongsToMany
    {
        return $this->BelongsToMany(Event::class, 'events_artists', 'artist_id', 'event_id');
    }

    public function social_media(): BelongsToMany
    {
        return $this->BelongsToMany(SocialMedia::class, 'artists_social_media', 'artist_id', 'social_media_id')->withPivot('link');
    }
}