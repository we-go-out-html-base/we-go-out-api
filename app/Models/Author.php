<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Author extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'authors';

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'pivot'
    ];

    protected $fillable = [
        'name',
        'biography',
        'image',
        'position'
    ];

    public function social_media(): BelongsToMany
    {
        return $this->BelongsToMany(SocialMedia::class, 'author_social_media', 'author_id', 'social_media_id')->withPivot('link');
    }

    public function news(): HasMany
    {
        return $this->HasMany(News::class, 'author_id');
    }
}
