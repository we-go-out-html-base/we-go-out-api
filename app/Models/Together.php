<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Together extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'togethers';

    protected $fillable = ['name', 'link', 'image', 'description', 'status', 'category_id', 'is_main', 'priority'];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function category(): BelongsTo
    {
        return $this->BelongsTo(TogetherCategory::class, 'category_id');
    }

    public function events(): BelongsToMany
    {
        return $this->BelongsToMany(Event::class, 'togethers_events', 'together_id', 'event_id');
    }


}
