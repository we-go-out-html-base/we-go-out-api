<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use DateTime;
use App\Models\Artist;
use App\Models\Event;
use App\Models\Release;
use App\Models\News;

class CreateSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:create-sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gera o arquivo sitemap.xml utilizado pelo site para mapeamento google';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $dateFormat = "Y-m-d\TH:i:sP";

        try {
        // exec('rm /var/www/we-go-out-frontend-nuxt/public/sitemap.xml');
        // $fileHandler = fopen("/Library/WebServer/Documents/we-go-out-api/public/XML/sitemap.xml","w+");
        $fileHandler = fopen("/var/www/we-go-out-frontend-nuxt/public/sitemap.xml","w+");
        @fwrite($fileHandler,'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">');
 
        @fwrite($fileHandler, '<url>
    <loc>https://wegoout.com.br/sobre</loc>
    <lastmod>2023-08-28T19:26:59+00:01</lastmod>
    <priority>1.0</priority>
    </url>
    <url>
    <loc>https://wegoout.com.br/consulta</loc>
    <lastmod>2023-08-28T19:26:59+00:02</lastmod>
    <priority>1.0</priority>
    </url>
    <url>
    <loc>https://wegoout.com.br/busca</loc>
    <lastmod>2023-08-28T19:26:59+00:03</lastmod>
    <priority>1.0</priority>
    </url>
    <url>
    <loc>https://wegoout.com.br/together</loc>
    <lastmod>2023-08-28T19:26:59+00:04</lastmod>
    <priority>1.0</priority>
    </url>
    <url>
    <loc>https://wegoout.com.br/artistas</loc>
    <lastmod>2023-08-28T19:26:59+00:05</lastmod>
    <priority>1.0</priority>
    </url>
    <url>
    <loc>https://wegoout.com.br/eventos</loc>
    <lastmod>2023-08-28T19:26:59+00:06</lastmod>
    <priority>1.0</priority>
    </url>
    <url>
    <loc>https://wegoout.com.br/lancamentos</loc>
    <lastmod>2023-08-28T19:26:59+00:07</lastmod>
    <priority>1.0</priority>
    </url>
    <url>
    <loc>https://wegoout.com.br/noticias</loc>
    <lastmod>2023-08-28T19:26:59+00:08</lastmod>
    <priority>1.0</priority>
    </url>
    <url>
    <loc>https://wegoout.com.br/politica-de-cookies</loc>
    <lastmod>2023-08-28T19:26:59+00:09</lastmod>
    <priority>1.0</priority>
    </url>
    <url>
    <loc>https://wegoout.com.br/politica-de-privacidade</loc>
    <lastmod>2023-08-28T19:26:59+00:10</lastmod>
    <priority>1.0</priority>
    </url>
        ');

        $artistas = Artist::all('updated_at', 'slug');
        foreach($artistas AS $artista){
            $formattedDate = new DateTime($artista->updated_at);
            $xml = "<url>\n    "; 
            $xml .= "<loc>https://wegoout.com.br/artistas/" . htmlspecialchars($artista->slug) . "</loc>\n    "; 
            $xml .= "<lastmod>". $formattedDate->format($dateFormat) . "</lastmod>\n    "; 
            $xml .= "<priority>0.8</priority>\n    "; 
            $xml .= "</url>\n    ";
            @fwrite($fileHandler, $xml);
        }

        $eventos = Event::all('updated_at', 'slug');
        foreach($eventos AS $evento){
            $formattedDate = new DateTime($evento->updated_at);
            $xml = "<url>\n    "; 
            $xml .= "<loc>https://wegoout.com.br/eventos/" . htmlspecialchars($evento->slug) . "</loc>\n    "; 
            $xml .= "<lastmod>". $formattedDate->format($dateFormat) . "</lastmod>\n    "; 
            $xml .= "<priority>0.8</priority>\n    "; 
            $xml .= "</url>\n    ";
            @fwrite($fileHandler, $xml);
        }

        $lancamentos = Release::all('updated_at', 'slug');
        foreach($lancamentos AS $lancamento){
            $formattedDate = new DateTime($lancamento->updated_at);
            $xml = "<url>\n    "; 
            $xml .= "<loc>https://wegoout.com.br/lancamentos/" . htmlspecialchars($lancamento->slug) . "</loc>\n    "; 
            $xml .= "<lastmod>". $formattedDate->format($dateFormat) . "</lastmod>\n    "; 
            $xml .= "<priority>0.8</priority>\n    "; 
            $xml .= "</url>\n    ";
            @fwrite($fileHandler, $xml);
        }

        $news = News::all('public_date', 'slug');
        foreach($news AS $new_){
            $formattedDate = new DateTime($new_->public_date);
            $xml =  "<url>\n    "; 
            $xml .= "<loc>https://wegoout.com.br/noticias/" . htmlspecialchars($new_->slug) . "</loc>\n    "; 
            $xml .= "<lastmod>". $formattedDate->format($dateFormat) . "</lastmod>\n    "; 
            $xml .= "<priority>0.8</priority>\n    "; 
            $xml .= "</url>\n    ";
            @fwrite($fileHandler, $xml);
        }

        @fwrite($fileHandler,"</urlset>"); 
        Log::info("Criou o sitemap");

        exec('cd /var/www/we-go-out-frontend-nuxt && yarn run build && pm2 restart wgo-frontend');

        // Executar o comando pm2 restart
        // exec('cd /var/www/we-go-out-frontend-nuxt && pm2 restart wgo-frontend');
    } catch(\Exception $error) {
        Log::error("Erro ao criar o sitemap: " . $error->getMessage());
    }

    }
}
