<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

use App\Http\Controllers\EventsController;
use App\Http\Controllers\LocationsController;
use App\Http\Controllers\ArtistsController;
use App\Http\Controllers\GenresController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\AdsController;
use App\Http\Controllers\AdsCategoriesController;
use App\Http\Controllers\HighlightsController;
use App\Http\Controllers\HighlightsTypesController;
use App\Http\Controllers\TogethersController;
use App\Http\Controllers\TogethersCategoriesController;
use App\Http\Controllers\ReleasesController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\LinkTreeController;
use App\Http\Controllers\AuthorsController;
use App\Http\Controllers\CitiesController;
use App\Http\Controllers\RoleAuthorController;
use App\Http\Controllers\CompaniesController;
use App\Http\Controllers\SiteDataController;
use App\Http\Controllers\StatesController;
use App\Http\Controllers\MassiveRedirectsController;
use App\Http\Controllers\FileUploadController;
use App\Mail\SubmittedEvent;
use App\Models\Event;
use App\Models\Release;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('dashboard');
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');





Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::resource('ads2', AdsController::class)->missing(function (Request $request) {
        return Redirect::route('ads.index');
    });

    Route::resource('/highlights', HighlightsController::class)->missing(function (Request $request) {
        return Redirect::route('highlights.index');
    });
    Route::resource('/highlightsTypes', HighlightsTypesController::class)->missing(function (Request $request) {
        return Redirect::route('highlightsTypes.index');
    });

    Route::resource('/togethers', TogethersController::class)->missing(function (Request $request) {
        return Redirect::route('togethers.index');
    });
    Route::resource('/togethersCategories', TogethersCategoriesController::class)->missing(function (Request $request) {
        return Redirect::route('togethersCategories.index');
    });

    Route::resource('adCategories', AdsCategoriesController::class)->missing(function (Request $request) {
        return Redirect::route('adCategories.index');
    });

    Route::resource('releases', ReleasesController::class)->missing(function (Request $request) {
        return Redirect::route('releases.index');
    });
    Route::post('/releases/{id}/activate', [ReleasesController::class, 'activate'])->name('releases.activate');

    Route::resource('events', EventsController::class)->missing(function (Request $request) {
        return Redirect::route('events.index');
    });
    Route::post('/events/{id}/activate', [EventsController::class, 'activate'])->name('event.activate');

    Route::resource('users', UsersController::class)->missing(function (Request $request) {
        return Redirect::route('users.index');
    });

    Route::resource('news', NewsController::class)->missing(function (Request $request) {
        return Redirect::route('news.index');
    });

    Route::resource('/genres', GenresController::class)->missing(function (Request $request) {
        return Redirect::route('genres.index');
    });
    Route::resource('/locations', LocationsController::class)->missing(function (Request $request) {
        return Redirect::route('locations.index');
    });

    Route::resource('pages', PagesController::class)->missing(function (Request $request) {
        return Redirect::route('pages.index');
    });

    Route::resource('artists', ArtistsController::class)->missing(function (Request $request) {
        return Redirect::route('artists.index');
    });

    Route::resource('linktree', LinkTreeController::class)->missing(function (Request $request) {
        return Redirect::route('linktree.index');
    });

    Route::resource('authors', AuthorsController::class)->missing(function (Request $request) {
        return Redirect::route('authors.index');
    });

    Route::resource('companies', CompaniesController::class)->missing(function (Request $request) {
        return Redirect::route('companies.index');
    });

    Route::resource('redirects', MassiveRedirectsController::class)->missing(function (Request $request) {
        return Redirect::route('redirects.index');
    });

    Route::resource('roleAuthor', RoleAuthorController::class)->missing(function (Request $request) {
        return Redirect::route('roleauthor.index');
    });

    Route::resource('states', StatesController::class)->missing(function (Request $request) {
        return Redirect::route('states.index');
    });

    Route::resource('cities', CitiesController::class)->missing(function (Request $request) {
        return Redirect::route('cities.index');
    });

    Route::resource('siteData', SiteDataController::class)->missing(function (Request $request) {
        return Redirect::route('siteData.index');
    });

    Route::get('emails/eventoCriado', function () {
        $titulo = "Título";
        return view('mails.submittedEvent', ['title' => $titulo, 'event' => Event::find(41)]);
    });

    Route::get('emails/eventoAtivado', function () {;
        $titulo = "Título";
        return view('mails.activatedEvent', ['title' => $titulo, 'event' => Event::find(41)]);
    });

    Route::get('emails/releaseCriado', function () {
        $titulo = "Título";
        return view('mails.submittedRelease', ['title' => $titulo, 'release' => Release::find(33)]);
    });

    Route::get('emails/releaseAtivado', function () {
        $titulo = "Título";
        return view('mails.activatedRelease', ['title' => $titulo, 'release' => Release::find(33)]);
    });

    Route::post('upload-image', [FileUploadController::class, 'uploadImage'])->middleware(['auth', 'verified'])->name('upload.image');
    Route::post('upload-extra-images', [FileUploadController::class, 'uploadExtraImages'])->middleware(['auth', 'verified'])->name('upload.extra.image');


    Route::get('logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);
});

require __DIR__ . '/auth.php';