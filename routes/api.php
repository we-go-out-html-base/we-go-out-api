<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\EventController;
use App\Http\Controllers\Api\ArtistController;
use App\Http\Controllers\Api\ReleaseController;
use App\Http\Controllers\Api\NewsController;
use App\Http\Controllers\Api\PagesController;
use App\Http\Controllers\Api\GeneralController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/events', [EventController::class, 'index']);
Route::post('/events/create', [EventController::class, 'create']);
Route::get('/cachedEventsList', [EventController::class, 'cachedEventsList']);
Route::get('/eventsForMap', [EventController::class, 'eventsForMap']);

Route::get('/artists', [ArtistController::class, 'index']);
Route::get('/releases', [ReleaseController::class, 'index']);
Route::post('/releases/create', [ReleaseController::class, 'create']);
Route::get('/cachedArtistsList', [ArtistController::class, 'cachedArtistsList']);
Route::get('/cachedReleasesList', [ReleaseController::class, 'cachedReleasesList']);

Route::get('/news', [NewsController::class, 'index']);
Route::get('/cachedListNews', [NewsController::class, 'cachedListNews']);

Route::get('/pages', [PagesController::class, 'index']);

Route::get('/countries', [GeneralController::class, 'listCountries']);
Route::get('/states', [GeneralController::class, 'listStates']);
Route::get('/cities', [GeneralController::class, 'listCities']);
Route::get('/citiesForMap', [GeneralController::class, 'citiesForMap']);
Route::get('/genres', [GeneralController::class, 'listGenres']);
Route::get('/locations', [GeneralController::class, 'listLocations']);
Route::get('/companies', [GeneralController::class, 'listCompanies']);
Route::get('/categories', [GeneralController::class, 'listCategories']);
Route::get('/eventTypes', [GeneralController::class, 'listEventTypes']);
Route::get('/tags', [GeneralController::class, 'listTags']);
Route::get('/highlights', [GeneralController::class, 'listHighlights']);
Route::get('/highlightsTypes', [GeneralController::class, 'listHighlightsTypes']);
Route::get('/ads', [GeneralController::class, 'listAds']);
Route::get('/users', [GeneralController::class, 'listUsers']);
Route::get('/authors', [GeneralController::class, 'listAuthors']);
Route::get('/adsCategories', [GeneralController::class, 'listAdsCategories']);
Route::get('/togethers', [GeneralController::class, 'listTogethers']);
Route::get('/togethersCategories', [GeneralController::class, 'listTogethersCategories']);
Route::get('/socialMedia', [GeneralController::class, 'listSocialMedia']);
Route::get('/linktree', [GeneralController::class, 'linktree']);
Route::get('/roleUser', [GeneralController::class, 'roleUser']);
Route::get('/redirect', [GeneralController::class, 'getRedirect']);
Route::get('/redirects', [GeneralController::class, 'listRedirects']);
Route::get('/siteData', [GeneralController::class, 'listSiteData']);
