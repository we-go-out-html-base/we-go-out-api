# WGO API USANDO DOCKER

## 1
Clonar esse repositório
```bash
https://gitlab.com/we-go-out-html-base/we-go-out-api.git
```

## 2
Mudar para a branch usingDocker
```bash
git  checkout  usingDocker
```

## 3
Copiar o arquivo .env.example para .env

## 4
Instalar composer e dependências
### 5 instalar o composer

```bash
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
````

```bash
php composer-setup.php
```

```bash
php composer.phar install
```

```bash
php artisan key:generate
```

## 5
### Iniciar o docker desktop

### Subir os containeres
```bash
docker-compose up --build
```
O argumento build só é necessário na primeira vez que é executado.


# Acesso
O wgo-api pode ser acessado pelo navegador em http://localhost:81
Login em http://localhost:81/login
API em http://localhost:81/api/
phpMyAdmin em http://localhost:8080


[//]: # (## Documentação)

[//]: # (Url: api/documentation)

## No servidor
Esse repositório é clonado em /var/www/we-go-out-api
O antigo /var/www/wgo-api deve ser apagado com o tempo. Por enquanto deixamos por uma reserva de emergência

### Sitemap.xml
Diariamente a api gera o arquivo sitemap.xml e coloca dentro da pasta /public do site. Essa geração é feita pela seguinte configuração cron:
```bash 
0 2 * * * cd /var/www/we-go-out-api && php artisan app:create-sitemap && cd /var/www/we-go-out-frontend-nuxt && yarn run build && pm2 restart wgo-frontend >> /var/log/cronjob.log 2>&1 
```
