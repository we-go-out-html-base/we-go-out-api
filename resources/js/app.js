import './bootstrap';
import '../css/app.css';

import { createApp, h } from 'vue';
import { createInertiaApp } from '@inertiajs/vue3';
import { resolvePageComponent } from 'laravel-vite-plugin/inertia-helpers';
import { ZiggyVue } from './libs/vue.m';

import {library} from '@fortawesome/fontawesome-svg-core';
import {faPenToSquare, faSearch, faTrash, faRoadCircleCheck} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome';
import Notification from './Components/Notification.vue'
import SlugInput from '@/Components/SlugInput.vue';

library.add(faPenToSquare, faSearch, faTrash, faRoadCircleCheck);

const appName = window.document.getElementsByTagName('title')[0]?.innerText || 'Laravel';

createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    resolve: (name) => resolvePageComponent(`./Pages/${name}.vue`, import.meta.glob('./Pages/**/*.vue')),
    setup({ el, App, props, plugin }) {
        return createApp({ render: () => h(App, props) })
            .use(plugin)
            .use(ZiggyVue, Ziggy)
            .component('font-awesome-icon', FontAwesomeIcon)
            .component('notification', Notification)
            .component('slugInput', SlugInput)
            .mount(el);
    },
    progress: {
        color: '#4B5563',
    },
});