let menuItemsForId1 = [
    {
        title: 'Usuários',
        icon: '/images/user-icon.svg',
        subItems: [
            { title: 'Lista', route: 'users.index' }
        ]
    },
    {
        title: 'Dados do site',
        icon: '/images/pages-icon.svg',
        subItems: [
            { title: 'Dados do site', route: 'siteData.index' }
        ]
    },
    {
        title: 'Eventos',
        icon: '/images/events-icon.svg',
        subItems: [
            { title: 'Lista de eventos', route: 'events.index' },
            { title: 'Locais', route: 'locations.index' },
            { title: 'Estados', route: 'states.index' },
            { title: 'Cidades', route: 'cities.index' }
        ]
    },
    {
        title: 'Artistas',
        icon: '/images/star-icon.svg',
        subItems: [
            { title: 'Lista de artistas', route: 'artists.index' },
            { title: 'Gêneros musicais', route: 'genres.index' },
            { title: 'Gravadoras', route: 'companies.index' }
        ]
    },
    {
        title: 'Notícias',
        icon: '/images/news-icon.svg',
        subItems: [
            { title: 'Lista', route: 'news.index' }
        ]
    },
    {
        title: 'Destaques',
        icon: '/images/agendas-icon.svg',
        subItems: [
            { title: 'Lista', route: 'highlights.index' },
            { title: 'Tipos', route: 'highlightsTypes.index' }
        ]
    },
    {
        title: 'Togethers',
        icon: '/images/togethers-icon.svg',
        subItems: [
            { title: 'Lista', route: 'togethers.index' },
            { title: 'Categorias', route: 'togethersCategories.index' }
        ]
    },
    {
        title: 'Anúncios',
        icon: '/images/anuc-icon.svg',
        subItems: [
            { title: 'Lista', route: 'ads2.index' },
            { title: 'Categorias', route: 'adCategories.index' }
        ]
    },
    {
        title: 'Lançamentos',
        icon: '/images/releases-icon.svg',
        subItems: [
            { title: 'Lista', route: 'releases.index' }
        ]
    },
    {
        title: 'Redirects',
        icon: '/images/redirect-icon.svg',
        subItems: [
            { title: 'Lista', route: 'redirects.index' }
        ]
    },
    {
        title: 'Páginas',
        icon: '/images/pages-icon.svg',
        subItems: [
            { title: 'Lista', route: 'pages.index' }
        ]
    },
    {
        title: 'LinkTree',
        icon: '/images/linktree-icon.svg',
        subItems: [
            { title: 'LinkTree', route: 'linktree.index' }
        ]
    },
    {
        title: 'Autores',
        icon: '/images/authors-icon.svg',
        subItems: [
            { title: 'Autores', route: 'authors.index' },
            { title: 'Cargos', route: 'roleAuthor.index' }
        ]
    }
];


// editor
const menuItemsForId2 = [
    {
        title: 'Eventos',
        icon: '/images/events-icon.svg',
        subItems: [
            { title: 'Lista de eventos', route: 'events.index' },
            { title: 'Locais', route: 'locations.index' },
            { title: 'Estados', route: 'states.index' },
            { title: 'Cidades', route: 'cities.index' }
        ]
    },
    {
        title: 'Artistas',
        icon: '/images/star-icon.svg',
        subItems: [
            { title: 'Lista de artistas', route: 'artists.index' },
            { title: 'Gêneros musicais', route: 'genres.index' },
            { title: 'Gravadoras', route: 'companies.index' }
        ]
    },
    {
        title: 'Notícias',
        icon: '/images/news-icon.svg',
        subItems: [
            { title: 'Lista', route: 'news.index' }
        ]
    },
    {
        title: 'Destaques',
        icon: '/images/agendas-icon.svg',
        subItems: [
            { title: 'Lista', route: 'highlights.index' },
            { title: 'Tipos', route: 'highlightsTypes.index' }
        ]
    },
    {
        title: 'Togethers',
        icon: '/images/togethers-icon.svg',
        subItems: [
            { title: 'Lista', route: 'togethers.index' },
            { title: 'Categorias', route: 'togethersCategories.index' }
        ]
    },
    {
        title: 'Lançamentos',
        icon: '/images/releases-icon.svg',
        subItems: [
            { title: 'Lista', route: 'releases.index' }
        ]
    },
]

// gestor de comunidade
const menuItemsForId3 = [
    {
        title: 'Eventos',
        icon: '/images/events-icon.svg',
        subItems: [
            { title: 'Lista de eventos', route: 'events.index' },
            { title: 'Locais', route: 'locations.index' },
            { title: 'Estados', route: 'states.index' },
            { title: 'Cidades', route: 'cities.index' }
        ]
    },
    {
        title: 'Notícias',
        icon: '/images/news-icon.svg',
        subItems: [
            { title: 'Lista', route: 'news.index' }
        ]
    },
    {
        title: 'Togethers',
        icon: '/images/togethers-icon.svg',
        subItems: [
            { title: 'Lista', route: 'togethers.index' },
            { title: 'Categorias', route: 'togethersCategories.index' }
        ]
    },
    {
        title: 'Anúncios',
        icon: '/images/anuc-icon.svg',
        subItems: [
            { title: 'Lista', route: 'ads2.index' },
            { title: 'Categorias', route: 'adCategories.index' }
        ]
    },
    {
        title: 'LinkTree',
        icon: '/images/linktree-icon.svg',
        subItems: [
            { title: 'LinkTree', route: 'linktree.index' }
        ]
    },
]


// social media
const menuItemsForId4 = [
    {
        title: 'Togethers',
        icon: '/images/togethers-icon.svg',
        subItems: [
            { title: 'Lista', route: 'togethers.index' },
            { title: 'Categorias', route: 'togethersCategories.index' }
        ]
    },
    {
        title: 'Anúncios',
        icon: '/images/anuc-icon.svg',
        subItems: [
            { title: 'Lista', route: 'ads2.index' },
            { title: 'Categorias', route: 'adCategories.index' }
        ]
    },
    {
        title: 'LinkTree',
        icon: '/images/linktree-icon.svg',
        subItems: [
            { title: 'LinkTree', route: 'linktree.index' }
        ]
    }
]

const menuItemsForDefault = [

]



export { menuItemsForId1, menuItemsForId2, menuItemsForId3, menuItemsForId4, menuItemsForDefault }
