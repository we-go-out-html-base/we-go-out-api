import ImageUploader from "quill-image-uploader";

const RichTextToolBar = [['bold', 'italic', 'underline', 'strike'], 
['blockquote', 'code-block'],

[{ 'header': 1 }, { 'header': 2 }],               
[{ 'list': 'ordered'}, { 'list': 'bullet' }],
[{ 'script': 'sub'}, { 'script': 'super' }],      
[{ 'indent': '-1'}, { 'indent': '+1' }],          
// [{ 'direction': 'rtl' }],                         

// [{ 'size': ['small', false, 'large', 'huge'] }],  
[{ 'header': [1, 2, 3, 4, 5, 6, false] }],

// [{ 'color': [] }, { 'background': [] }],          
[{ 'font': [] }],
[{ 'align': [] }],

['link', 'image', 'video']]

const RichTextModules = {
  name: 'imageUploader',
  module: ImageUploader,
  options: {
    upload: file => {
      return new Promise((resolve, reject) => {
        const formData = new FormData();
        formData.append("image", file);
        formData.append('filePath', 'uploadedImages')
        axios.post('/upload-extra-images', formData, {
              headers: {
                  'Content-Type': 'multipart/form-data'
              }
          })

        .then(res => {
          console.log(res)
          resolve(res.data);
        })
        .catch(err => {
          reject("Erro ao inserir a imagem");
          console.error("Error:", err)
        })
      })
    }
}
}

export { RichTextToolBar, RichTextModules }
// export RichTextModules